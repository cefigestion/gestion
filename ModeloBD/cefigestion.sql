-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-08-2017 a las 22:40:50
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cefigestion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acciones`
--

CREATE TABLE `acciones` (
  `id` int(11) NOT NULL,
  `tipoaccion` text NOT NULL,
  `fechahora` varchar(50) NOT NULL,
  `idusuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `acciones`
--

INSERT INTO `acciones` (`id`, `tipoaccion`, `fechahora`, `idusuario`) VALUES
(60, 'Ingreso al sistema', '29/Apr/2017  -  3:03', 13),
(61, 'El usuario agrego el usuario Administrador asd, asd al sistema.', '29/Apr/2017  -  3:03', 13),
(62, 'El usuario agrego el usuario Vendedor asd, asd al sistema.', '29/Apr/2017  -  3:03', 13),
(63, 'Ingreso al sistema', '29/Apr/2017  -  4:04', 13),
(64, 'Ingreso al sistema', '29/Apr/2017  -  4:04', 13),
(65, 'El usuario agregÃ³ el producto masitas al sistema.', '29/Apr/2017  -  4:04', 13),
(66, 'El usuario agregÃ³ el producto mas al sistema.', '29/04/2017  -  4:04', 13),
(67, 'El usuario agregÃ³ el producto asd al sistema.', '29/04/2017  -  4:04', 13),
(68, 'El usuario elimino el producto mas del sistema.', '29/04/2017  -  04:52', 13),
(69, 'Ingreso al sistema', '29/04/2017  -  04:54', 13),
(70, 'Ingreso al sistema', '29/04/2017  -  05:42', 13),
(71, 'Ingreso al sistema', '29/04/2017  -  05:45', 13),
(72, 'Ingreso al sistema', '29/04/2017  -  05:52', 13),
(73, 'Ingreso al sistema', '29/04/2017  -  06:05', 21),
(74, 'RealizÃ³ retiro de un total de 120 pesos.', '29/04/2017  -  06:29', 21),
(75, 'Ingreso al sistema', '29/04/2017  -  06:33', 13),
(76, 'El usuario agregÃ³ el producto otro al sistema.', '29/04/2017  -  06:34', 13),
(77, 'Ingreso al sistema', '29/04/2017  -  20:14', 13),
(78, 'Ingreso al sistema', '29/04/2017  -  20:24', 13),
(79, 'Ingreso al sistema', '29/04/2017  -  20:24', 13),
(80, 'Ingreso al sistema', '29/04/2017  -  20:24', 13),
(81, 'Ingreso al sistema', '29/04/2017  -  20:25', 13),
(82, 'Ingreso al sistema', '29/04/2017  -  23:25', 13),
(83, 'Ingreso al sistema', '29/04/2017  -  23:35', 13),
(84, 'Ingreso al sistema', '29/04/2017  -  23:49', 13),
(85, 'El usuario agregÃ³ el producto p1 al sistema.', '30/04/2017  -  00:56', 13),
(86, 'El usuario agregÃ³ el producto p2 al sistema.', '30/04/2017  -  00:56', 13),
(87, 'El usuario agregÃ³ el producto p3 al sistema.', '30/04/2017  -  00:56', 13),
(88, 'El usuario agregÃ³ el producto p4 al sistema.', '30/04/2017  -  00:56', 13),
(89, 'El usuario agregÃ³ el producto p5 al sistema.', '30/04/2017  -  00:56', 13),
(90, 'El usuario agregÃ³ el producto p6 al sistema.', '30/04/2017  -  00:56', 13),
(91, 'Ingreso al sistema', '30/04/2017  -  01:32', 13),
(92, 'El usuario agregÃ³ el producto s1 al sistema.', '30/04/2017  -  01:57', 13),
(93, 'El usuario agregÃ³ el producto s12 al sistema.', '30/04/2017  -  01:57', 13),
(94, 'El usuario agregÃ³ el producto s3 al sistema.', '30/04/2017  -  01:57', 13),
(95, 'El usuario agregÃ³ el producto sasd4 al sistema.', '30/04/2017  -  01:57', 13),
(96, 'El usuario agregÃ³ el producto asdf56 al sistema.', '30/04/2017  -  01:57', 13),
(97, 'El usuario agregÃ³ el producto asdas231 al sistema.', '30/04/2017  -  01:57', 13),
(98, 'Ingreso al sistema', '30/04/2017  -  02:25', 13),
(99, 'Ingreso al sistema', '01/05/2017  -  00:36', 13),
(100, 'Ingreso al sistema', '01/05/2017  -  00:41', 13),
(101, 'El usuario elimino el producto masitas del sistema.', '01/05/2017  -  02:29', 13),
(102, 'El usuario elimino el producto masitas del sistema.', '01/05/2017  -  02:29', 13),
(103, 'El usuario elimino el producto productoprueba del sistema.', '01/05/2017  -  02:29', 13),
(104, 'El usuario elimino el producto otro del sistema.', '01/05/2017  -  02:29', 13),
(105, 'El usuario elimino el producto p2 del sistema.', '01/05/2017  -  02:29', 13),
(106, 'El usuario elimino el producto asd del sistema.', '01/05/2017  -  02:29', 13),
(107, 'El usuario elimino el producto productootro del sistema.', '01/05/2017  -  02:29', 13),
(108, 'El usuario elimino el producto p3 del sistema.', '01/05/2017  -  02:29', 13),
(109, 'El usuario elimino el producto p1 del sistema.', '01/05/2017  -  02:29', 13),
(110, 'El usuario elimino el producto s12 del sistema.', '01/05/2017  -  02:29', 13),
(111, 'El usuario elimino el producto s3 del sistema.', '01/05/2017  -  02:29', 13),
(112, 'El usuario elimino el producto p4 del sistema.', '01/05/2017  -  02:29', 13),
(113, 'El usuario elimino el producto p6 del sistema.', '01/05/2017  -  02:29', 13),
(114, 'El usuario elimino el producto s1 del sistema.', '01/05/2017  -  02:30', 13),
(115, 'El usuario elimino el producto asdf56 del sistema.', '01/05/2017  -  02:30', 13),
(116, 'Ingreso al sistema', '01/05/2017  -  21:21', 13),
(117, 'Ingreso al sistema', '01/05/2017  -  22:02', 13),
(118, 'Ingreso al sistema', '01/05/2017  -  22:02', 13),
(119, 'Ingreso al sistema', '01/05/2017  -  22:02', 13),
(120, 'Ingreso al sistema', '01/05/2017  -  22:02', 13),
(121, 'Ingreso al sistema', '01/05/2017  -  22:02', 13),
(122, 'Ingreso al sistema', '01/05/2017  -  22:02', 13),
(123, 'Ingreso al sistema', '01/05/2017  -  22:02', 13),
(124, 'Ingreso al sistema', '01/05/2017  -  22:03', 13),
(125, 'Ingreso al sistema', '01/05/2017  -  22:03', 13),
(126, 'Ingreso al sistema', '01/05/2017  -  22:03', 13),
(127, 'Ingreso al sistema', '01/05/2017  -  22:03', 13),
(128, 'Ingreso al sistema', '01/05/2017  -  22:03', 13),
(129, 'Ingreso al sistema', '01/05/2017  -  22:03', 13),
(130, 'Ingreso al sistema', '01/05/2017  -  22:03', 13),
(131, 'Ingreso al sistema', '01/05/2017  -  23:10', 13),
(132, 'Ingreso al sistema', '02/05/2017  -  00:01', 13),
(133, 'Ingreso al sistema', '02/05/2017  -  14:04', 13),
(134, 'Ingreso al sistema', '02/05/2017  -  14:04', 13),
(135, 'Ingreso al sistema', '02/05/2017  -  14:04', 13),
(136, 'Ingreso al sistema', '03/05/2017  -  23:11', 13),
(137, 'Ingreso al sistema', '03/05/2017  -  23:16', 13),
(138, 'Ingreso al sistema', '03/05/2017  -  23:16', 13),
(139, 'Ingreso al sistema', '03/05/2017  -  23:37', 13),
(140, 'El usuario agregÃ³ el producto ultimo al sistema.', '04/05/2017  -  00:16', 13),
(141, 'El usuario agregÃ³ el producto ultimo1 al sistema.', '04/05/2017  -  00:17', 13),
(142, 'El usuario agregÃ³ el producto ultimo2 al sistema.', '04/05/2017  -  00:17', 13),
(143, 'El usuario agregÃ³ el producto ultimo3 al sistema.', '04/05/2017  -  00:17', 13),
(144, 'El usuario agrego el usuario Administrador sdasdqdwdeqwe, sadasfafs al sistema.', '04/05/2017  -  00:18', 13),
(145, 'El usuario agregÃ³ el producto 2 al sistema.', '04/05/2017  -  00:35', 13),
(146, 'El usuario agregÃ³ el producto 231 al sistema.', '04/05/2017  -  00:38', 13),
(147, 'El usuario agrego el usuario Vendedor asdadsasdads, asfasdgvd al sistema.', '04/05/2017  -  00:39', 13),
(148, 'El usuario agrego el usuario Vendedor asdasdsad, asd al sistema.', '04/05/2017  -  00:41', 13),
(149, 'Ingreso al sistema', '04/05/2017  -  00:43', 13),
(150, 'El usuario agrego el usuario Vendedor as, as al sistema.', '04/05/2017  -  00:44', 13),
(151, 'El usuario agrego el usuario Vendedor asasdasdasd, asdasdasdfsd al sistema.', '04/05/2017  -  00:45', 13),
(152, 'Ingreso al sistema', '04/05/2017  -  21:20', 13),
(153, 'Ingreso al sistema', '10/05/2017  -  11:35', 13),
(154, 'Ingreso al sistema', '10/05/2017  -  11:43', 13),
(155, 'Ingreso al sistema', '20/05/2017  -  21:11', 13),
(156, 'Ingreso al sistema', '10/07/2017  -  22:23', 13),
(157, 'Ingreso al sistema', '11/07/2017  -  05:01', 13),
(158, 'Ingreso al sistema', '11/07/2017  -  05:02', 13),
(159, 'Ingreso al sistema', '11/07/2017  -  00:05', 13),
(160, 'Ingreso al sistema', '11/07/2017  -  00:32', 13),
(161, 'Ingreso al sistema', '11/07/2017  -  00:53', 13),
(162, 'Ingreso al sistema', '19/07/2017  -  21:22', 13),
(163, 'Ingreso al sistema', '19/07/2017  -  21:22', 13),
(164, 'Ingreso al sistema', '19/07/2017  -  21:22', 24),
(165, 'Ingreso al sistema', '19/07/2017  -  21:23', 13),
(166, 'Ingreso al sistema', '11/08/2017  -  17:42', 13),
(167, 'Ingreso al sistema', '11/08/2017  -  20:16', 13),
(168, 'Ingreso al sistema', '11/08/2017  -  20:17', 13),
(169, 'Ingreso al sistema', '11/08/2017  -  21:28', 13),
(170, 'Ingreso al sistema', '11/08/2017  -  21:29', 13),
(171, 'Ingreso al sistema', '11/08/2017  -  21:55', 13),
(172, 'Ingreso al sistema', '11/08/2017  -  22:02', 13),
(173, 'Ingreso al sistema', '12/08/2017  -  16:07', 13),
(174, 'Ingreso al sistema', '12/08/2017  -  16:38', 13),
(175, 'Ingreso al sistema', '12/08/2017  -  16:57', 13),
(176, 'Ingreso al sistema', '12/08/2017  -  16:58', 13),
(177, 'Ingreso al sistema', '12/08/2017  -  16:59', 13),
(178, 'Ingreso al sistema', '12/08/2017  -  17:02', 13),
(179, 'Ingreso al sistema', '12/08/2017  -  17:06', 13),
(180, 'Ingreso al sistema', '12/08/2017  -  17:15', 13),
(181, 'Ingreso al sistema', '12/08/2017  -  17:17', 13),
(182, 'Ingreso al sistema', '12/08/2017  -  17:39', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajas`
--

CREATE TABLE `cajas` (
  `id` int(11) NOT NULL,
  `cerrada` tinyint(4) NOT NULL,
  `fechahoraapertura` datetime DEFAULT NULL,
  `fechahoracierre` datetime DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `montoinicial` float NOT NULL,
  `montofinal` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cajas`
--

INSERT INTO `cajas` (`id`, `cerrada`, `fechahoraapertura`, `fechahoracierre`, `idusuario`, `montoinicial`, `montofinal`) VALUES
(4, 1, '2017-04-29 06:06:15', '2017-04-29 06:30:19', 21, 350, 400),
(5, 1, '2017-07-19 21:22:31', '2017-07-19 21:22:54', 24, 300, 400);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` int(11) NOT NULL,
  `idproductocompra` int(11) NOT NULL,
  `idproveedores` int(11) NOT NULL,
  `idusuarios` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `preciocompra` float NOT NULL,
  `cantidadactual` int(11) NOT NULL,
  `precioventa` float NOT NULL,
  `borrado` tinyint(4) NOT NULL DEFAULT '0',
  `comision` float NOT NULL,
  `tipoventa` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `preciocompra`, `cantidadactual`, `precioventa`, `borrado`, `comision`, `tipoventa`) VALUES
(8, 'masitas', 123, 0, 151.29, 0, 23, 0),
(9, 'mas', 123, 7, 151.29, 0, 23, 0),
(10, 'asd', 0, 0, 2, 0, 0, 0),
(23, 'productoprueba', 13, 14, 20, 0, 0.99, 0),
(34, 'productootro', 13, 28, 15, 0, 0.8, 0),
(35, 'otro', 12, 10, 20, 0, 66.67, 0),
(36, 'p1', 0, 0, 1, 0, 0, 0),
(37, 'p2', 0, 0, 2, 0, 0, 0),
(38, 'p3', 0, 0, 3, 0, 0, 0),
(39, 'p4', 0, 0, 4, 0, 0, 0),
(40, 'p5', 0, 0, 5, 0, 0, 0),
(41, 'p6', 0, 0, 5, 0, 0, 0),
(42, 's1', 0, 0, 12, 0, 0, 0),
(43, 's12', 0, 0, 2, 0, 0, 0),
(44, 's3', 0, 0, 3, 0, 0, 0),
(45, 'sasd4', 0, 0, 4, 0, 0, 0),
(46, 'asdf56', 0, 0, 5, 0, 0, 0),
(47, 'asdas231', 0, 0, 3, 0, 0, 0),
(48, 'ultimo', 0, 0, 12, 0, 0, 0),
(49, 'ultimo1', 0, 0, 2, 0, 0, 1),
(50, 'ultimo2', 0, 0, 3, 0, 0, 0),
(51, 'ultimo3', 0, 0, 4, 0, 0, 1),
(52, '2', 0, 0, 44, 0, 0, 1),
(53, '231', 0, 0, 23, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoscompras`
--

CREATE TABLE `productoscompras` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `preciocompra` float NOT NULL,
  `idproducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosproveedores`
--

CREATE TABLE `productosproveedores` (
  `id` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productosproveedores`
--

INSERT INTO `productosproveedores` (`id`, `idproveedor`, `idproducto`) VALUES
(22, 1, 8),
(23, 1, 9),
(24, 1, 10),
(25, 1, 35),
(26, 1, 40),
(27, 1, 23),
(28, 1, 34);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productosventas`
--

CREATE TABLE `productosventas` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precioventa` float NOT NULL,
  `idventas` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productosventas`
--

INSERT INTO `productosventas` (`id`, `cantidad`, `precioventa`, `idventas`, `idproductos`) VALUES
(5, 3, 20, 4, 23),
(6, 9, 20, 5, 23),
(7, 2, 15, 5, 34),
(8, 4, 20, 6, 23),
(9, 4, 20, 6, 35);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id` int(11) NOT NULL,
  `razonsocial` varchar(45) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `borrado` tinyint(4) NOT NULL DEFAULT '0',
  `cuit` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id`, `razonsocial`, `telefono`, `email`, `borrado`, `cuit`) VALUES
(1, 'name', '123123', 'asdasd', 0, '12355555555'),
(2, '', '123123', 'asdasd', 1, '123'),
(3, 'proveedor 1', '343434', 'algo@algo', 0, '12252525251'),
(4, 'proov2', '0222222', 'algo@algo.com', 0, '12121212555'),
(5, 'proov3', '454545', 'algo', 0, '12121212121'),
(6, 'ulti', '232332', 'algo.com', 0, '22222222222');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registroscajas`
--

CREATE TABLE `registroscajas` (
  `id` int(11) NOT NULL,
  `mensaje` text NOT NULL,
  `idcaja1` int(11) NOT NULL,
  `idcaja2` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `registroscajas`
--

INSERT INTO `registroscajas` (`id`, `mensaje`, `idcaja1`, `idcaja2`, `fecha`) VALUES
(1, 'Al abrir la caja se encontro una discrepancia entre el monto final de la caja anterior, y el monto incial de la caja nueva.', 5, 4, '2017-07-19 21:22:31'),
(2, 'Al cierre de la caja se encontro una discrepancia entre el saldo de la caja y el saldo total de ventas.', 5, NULL, '2017-07-19 21:22:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `retiros`
--

CREATE TABLE `retiros` (
  `id` int(11) NOT NULL,
  `monto` float NOT NULL,
  `fecha` datetime NOT NULL,
  `cajas_id` int(11) NOT NULL,
  `usuarios_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `retiros`
--

INSERT INTO `retiros` (`id`, `monto`, `fecha`, `cajas_id`, `usuarios_id`) VALUES
(2, 120, '2017-04-29 06:29:02', 4, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `pass` varchar(45) NOT NULL,
  `bloqueado` tinyint(4) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `dni` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `observacion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `pass`, `bloqueado`, `nombre`, `apellido`, `dni`, `tipo`, `observacion`) VALUES
(13, 'asd', 0, 'santiago', 'figgini', 1, 0, ''),
(20, 'asd', 0, 'asd', 'asd', 1233, 0, ''),
(21, 'asd', 0, 'asd', 'asd', 12334, 1, 'Se portÃ³ mal'),
(22, 'sd', 0, 'sdasdqdwdeqwe', 'sadasfafs', 12, 0, ''),
(23, 'as', 0, 'asdadsasdads', 'asfasdgvd', 12314, 1, ''),
(24, 'asd', 0, 'asdasdsad', 'asd', 1234, 1, ''),
(25, 'asd', 0, 'as', 'as', 1234567876, 1, ''),
(26, 'asd', 0, 'asasdasdasd', 'asdasdasdfsd', 2147483647, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `total` float NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `cancelar` tinyint(4) NOT NULL DEFAULT '0',
  `idcaja` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `total`, `fecha`, `hora`, `cancelar`, `idcaja`) VALUES
(4, 60, '2017-04-29', '06:11:44', 0, 4),
(5, 210, '2017-04-29', '06:16:48', 0, 4),
(6, 160, '2017-07-19', '21:22:43', 0, 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acciones`
--
ALTER TABLE `acciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_acciones_usuarios1_idx` (`idusuario`);

--
-- Indices de la tabla `cajas`
--
ALTER TABLE `cajas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cajas_usuarios1_idx` (`idusuario`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_compras_productoscompras1_idx` (`idproductocompra`),
  ADD KEY `fk_compras_proveedores1_idx` (`idproveedores`),
  ADD KEY `fk_compras_usuarios1_idx` (`idusuarios`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_UNIQUE` (`nombre`);

--
-- Indices de la tabla `productoscompras`
--
ALTER TABLE `productoscompras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_productoscompras_productos1_idx` (`idproducto`);

--
-- Indices de la tabla `productosproveedores`
--
ALTER TABLE `productosproveedores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_productosproveedores_proveedores1_idx` (`idproveedor`),
  ADD KEY `fk_productosproveedores_productos1_idx` (`idproducto`);

--
-- Indices de la tabla `productosventas`
--
ALTER TABLE `productosventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_prodductosventas_ventas1_idx` (`idventas`),
  ADD KEY `fk_prodductosventas_productos1_idx` (`idproductos`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `registroscajas`
--
ALTER TABLE `registroscajas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `retiros`
--
ALTER TABLE `retiros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_retiros_cajas1_idx` (`cajas_id`),
  ADD KEY `fk_retiros_usuarios1_idx` (`usuarios_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni_UNIQUE` (`dni`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ventas_cajas1_idx` (`idcaja`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acciones`
--
ALTER TABLE `acciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;
--
-- AUTO_INCREMENT de la tabla `cajas`
--
ALTER TABLE `cajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de la tabla `productoscompras`
--
ALTER TABLE `productoscompras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productosproveedores`
--
ALTER TABLE `productosproveedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `productosventas`
--
ALTER TABLE `productosventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `registroscajas`
--
ALTER TABLE `registroscajas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `retiros`
--
ALTER TABLE `retiros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acciones`
--
ALTER TABLE `acciones`
  ADD CONSTRAINT `fk_acciones_usuarios1` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cajas`
--
ALTER TABLE `cajas`
  ADD CONSTRAINT `fk_cajas_usuarios1` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `fk_compras_productoscompras1` FOREIGN KEY (`idproductocompra`) REFERENCES `productoscompras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compras_proveedores1` FOREIGN KEY (`idproveedores`) REFERENCES `proveedores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compras_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productoscompras`
--
ALTER TABLE `productoscompras`
  ADD CONSTRAINT `fk_productoscompras_productos1` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productosproveedores`
--
ALTER TABLE `productosproveedores`
  ADD CONSTRAINT `fk_productosproveedores_productos1` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_productosproveedores_proveedores1` FOREIGN KEY (`idproveedor`) REFERENCES `proveedores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productosventas`
--
ALTER TABLE `productosventas`
  ADD CONSTRAINT `fk_prodductosventas_productos1` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_prodductosventas_ventas1` FOREIGN KEY (`idventas`) REFERENCES `ventas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `retiros`
--
ALTER TABLE `retiros`
  ADD CONSTRAINT `fk_retiros_cajas1` FOREIGN KEY (`cajas_id`) REFERENCES `cajas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retiros_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_ventas_cajas1` FOREIGN KEY (`idcaja`) REFERENCES `cajas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
