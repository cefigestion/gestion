<?php
session_start();

require_once('Clases/Persistencia/ConexionBD.php');

date_default_timezone_set('America/Argentina/Buenos_Aires');  //Esta linea es para que se guarde la hora bien, de acuerdo al horario de acá
//Orden sera la orden enviada por ajax por el parametro t
$orden=$_POST['t'];

//Inicializo respuesta
$respuesta=array('err'=>0,'txerr'=>'');

//Pedimos instancia DB
$con=ConexionBD::obtenerInstancia();

//SWITCH DE CONTROL
switch($orden){
  //--------------------------------------------------------------------------------------------------- Comprobacion de usuarios y sesion ------------------
    case 'ingresar':
            require_once("Clases/Logica/Accion.php");

            $dni=$_POST['dni'];
            $contrasena=$_POST['contrasena'];
            //VERIFICO QUE EXISTA EL USUARIO
            $sql="select count(*) as cant from usuarios where dni='$dni' and pass='$contrasena'";
            $registro=ConexionBD::obtenerInstancia()->consultar($sql);
            $array_user=armarArrayCon($registro)[0];
            if(isset($_SESSION['dni'])){
                session_destroy();
              }

            if($array_user['cant']>0)
            {

                $sql="select * from usuarios where dni='$dni' and pass='$contrasena'";
                $registro=ConexionBD::obtenerInstancia()->consultar($sql);
                $array_user=armarArrayCon($registro)[0];
                if($array_user['bloqueado']==1)
                {

                    $mensaje="Este usuario a sido bloqueado, por favor contactarse con el CEFI.";
                    $error=4;
                  
                }
                else
                {
                  $_SESSION['tipo']=$array_user['tipo'];
                  $_SESSION['dni']=$array_user['dni'];
                  $_SESSION['id']=$array_user['id'];
                  $mensaje="";
                  $error=0;

                  //--- Guardo el reporte de ingreso al sistema
                  $a = new Accion();
                  $a->setIdUsuario($_SESSION['id']);
                  $accion = "Ingreso al sistema";
                  $a->setAccion($accion);
                  $a->setFechaHora(date("d") . "/" . date("m") . "/" . date("Y") . "  -  " . date("H") . ":" . date("i"));
                  $a->guardar();

                  //-------- Limpio las acciones viejas
                  require_once("Clases/Logica/Usuario.php");
                  $usuario = new Usuario();
                  $usuario->limpiarAcciones($_SESSION['id']);

                }
              
            }
            else {
              $mensaje="Combinación de usuario y contraseña incorrecta, intente de nuevo.";
              $error=2;
            }

          $respuesta=array('err'=>$error, 'msg'=>$mensaje);

          break;  
  case 'logout':
          if($_SESSION['dni']!=''){
            $_SESSION=array();
            session_destroy();
            $respuesta=array('err'=>0, 'state'=>true);
          }
          else {
            $respuesta=array('err'=>1, 'state'=>false);
          }
          break;

  case "cerrarSesion":
      //LOGOUT
      $_SESSION=array();
      session_destroy();
      break;

  case "checkUser":
            if(isset($_SESSION['dni']))
            {
              switch ($_SESSION['tipo']) {
                case '0'://ADMIN
                  $respuesta=array('err'=>0, 'state'=>true, 'permiso'=>'Administrador');
                  break;
                case '1'://VENDEDOR
                  include('Clases/Logica/Caja.php');
                  $existeCaja=Caja::cajaAbiertaSegunIdUsuario($_SESSION['id']);
                  $respuesta=array('err'=>0, 'state'=>true, 'permiso'=>'Vendedor', 'existeCaja'=> "$existeCaja");
                  break;
                
              }
            }
            else {
              $respuesta=array('err'=>1, 'state'=>false);
            }
            break;


  //--------------------------------------------------------------------------------------------------- Terminal de ventas ------------------
    case "abrirCaja":
            if(isset($_SESSION['dni']) && $_SESSION['tipo']==1)
            {
              include('Clases/Logica/Caja.php');
              if(Caja::hayAbierta())
                $respuesta=array('err'=>2, 'msg'=> 'Ya se encuentra una caja abierta.');
              else if ($_POST['montoInicial']<=0) {
                $respuesta=array('err'=>3, 'msg'=> 'Monto inicial invalido.');
              }
              else{
                $montoInicial=$_POST['montoInicial'];
                $idUsuario=$_SESSION['id'];
                $rta=Caja::abrirCaja($montoInicial,$idUsuario);
                if($rta!=true)
                  $respuesta=array('err'=>1, 'msg'=> $rta);
              }
            }
            else
              $respuesta=array('err'=>1, 'msg'=> 'No hay sesion abierta.');
            break;

    case "cerrarCaja":
        if(isset($_SESSION['dni']) && $_SESSION['tipo']==1)
        {
          include('Clases/Logica/Caja.php');
          $montoFinal=$_POST['montoFinal'];
          Caja::cerrarCaja($montoFinal);

          //LOGOUT
          $_SESSION=array();
          session_destroy();
        }
        else
          $respuesta=array('err'=>1, 'msg'=> 'No hay sesion abierta.');
        break;

    case "cerrarVenta":
        include('Clases/Logica/Producto.php');
        include('Clases/Logica/Venta.php');
        include('Clases/Logica/ProductoVenta.php');
        include('Clases/Logica/Caja.php');

        $productos=json_decode($_POST['productos']);
        $cantidades=json_decode($_POST['cantidades']);

        $p= new Producto();
        $total=0;
        for($i=0;$i<sizeof($productos);$i++)
        {
          $p->setIdProducto($productos[$i]);
          $producto=armarArrayCon($p->traerProducto());
          $total+=$producto[0]['precioventa']*$cantidades[$i];
        }

        $v=new Venta();
        $v->inicializar();
        $v->setIdCaja(Caja::idCajaAbierta());
        $v->setTotal($total);
        $v->guardar();
        $id=$v->getId();

        $p= new Producto();
        for($i=0;$i<sizeof($productos);$i++)
        {
          $pv= new ProductoVenta(null,$id,$productos[$i]);

          $p->setIdProducto($productos[$i]);

          $pv->setPrecioVenta(armarArrayCon($p->traerProducto())[0]['precioventa']);
          $pv->setCantidad($cantidades[$i]);
          $pv->guardar();

          $nuevacant=armarArrayCon($p->traerProducto())[0]['cantidadactual']*1-$cantidades[$i];
          $p->setCantidadProducto($nuevacant);
          $p->actualizarCantidad();
        }
        break;

    case "guardarRetiro":
        include('Clases/Logica/Retiro.php');
        include('Clases/Logica/Caja.php');
        require_once("Clases/Logica/Accion.php");
                $dni=$_POST['dni'];
                $contraseña=$_POST['contraseña'];
                //VERIFICO QUE EXISTA EL USUARIO
                $sql="select count(*) as cant from usuarios where dni='$dni' and pass='$contraseña'";
                $registro=ConexionBD::obtenerInstancia()->consultar($sql);
                $array_user=armarArrayCon($registro)[0];
                if(!isset($_SESSION['dni'])){
                        $mensaje="No hay usuario solicitador.";
                        $error=6;
                  }
                else if($array_user['cant']>0)
                {

                    $sql="select * from usuarios where dni='$dni' and pass='$contraseña'";
                    $registro=ConexionBD::obtenerInstancia()->consultar($sql);
                    $array_user=armarArrayCon($registro)[0];
                    if($array_user['bloqueado']==1)
                    {

                        $mensaje="Este usuario a sido bloqueado, por favor contactarse con el CEFI.";
                        $error=4;
                      
                    }
                    else if($array_user['tipo']!=0)
                    {

                        $mensaje="Este usuario no tiene acceso a Retiros.";
                        $error=5;
                      
                    }
                    else
                    {
                        $r= new Retiro();
                        $r->inicializar();
                        $r->setIdCaja(Caja::idCajaAbierta());
                        $r->setIdUsuario($array_user['id']);
                        $r->guardar();
                      //--- Guardo el reporte de ingreso al sistema
                      $a = new Accion();
                      $a->setIdUsuario($_SESSION['id']);
                      $accion = "Realizó retiro de un total de " . $_POST['montoRetiro']. " pesos." ;
                      $a->setAccion($accion);
                      $a->setFechaHora(date("d") . "/" . date("m") . "/" . date("Y") . "  -  " . date("H") . ":" . date("i"));
                      $a->guardar();
                        $error=0;
                        $mensaje='Todo Ok';

                    }
                  
                }
                else {
                  $mensaje="Combinación de usuario y contraseña incorrecta, intente de nuevo.";
                  $error=2;
                }

              $respuesta=array('err'=>$error, 'msg'=>$mensaje);

         break;
    case "initTerminalVenta":
          if(isset($_SESSION['dni']) && $_SESSION['tipo']==1)
          {
            include('Clases/Logica/Producto.php');
            $productos=Producto::traerTodos();
            $respuesta=array('err'=>'0', 'productos'=> armarArrayCon($productos));
          }
          else
            $respuesta=array('err'=>1, 'msg'=> 'No hay sesion abierta.');
          break;
    case "trearProximaVenta":
            require_once("Clases/Logica/Venta.php");
            $id = Venta::trearProximaVenta();
            $respuesta=array('err'=>0, 'numero'=> $id);
            break;

  //--------------------------------------------------------------------------------------------------- TerminalCompra ------------------

    case "traerTodosProveedores":
            require_once("Clases/Logica/Proveedor.php");
            $proveedores = Proveedor::traerTodos();
            $respuesta=array('err'=>0, 'proveedores'=> armarArrayCon($proveedores));
            break;
    case "initTerminalCompra":
          if(isset($_SESSION['dni']) && $_SESSION['tipo']==0)
          {
            include('Clases/Logica/Producto.php');
            $productos=Producto::traerTodosPorProveedor($_POST['idProveedor']);
            $respuesta=array('err'=>'0', 'productos'=> armarArrayCon($productos));
          }
          else
            $respuesta=array('err'=>1, 'msg'=> 'No hay sesion abierta.');
          break;

    case "cerrarCompra":
        include('Clases/Logica/Producto.php');
        include('Clases/Logica/Compra.php');
        include('Clases/Logica/ProductoCompra.php');
        //include('Clases/Logica/Caja.php');

        $productos=json_decode($_POST['productos']);
        $cantidades=json_decode($_POST['cantidades']);
        $precios=json_decode($_POST['precios']);
        $proveedor=$_POST['idProveedor'];

        $p= new Producto();
        $total=0;
        for($i=0;$i<sizeof($productos);$i++)
        {
          $p->setIdProducto($productos[$i]);
          $p->setPrecioCompraProducto($precios[$i]);
          $producto=armarArrayCon($p->traerProducto());
          $total+=$precios[$i]*$cantidades[$i];
        }

        $c=new Compra();
        $c->inicializar();
        //$c->setIdCaja(Caja::idCajaAbierta());
        $c->setTotal($total);
        $c->setIdProveedores($proveedor);
        $c->setIdUsuarios($_SESSION['id']);
        $c->guardar();
        $id=$c->getId();

        $p= new Producto();
        for($i=0;$i<sizeof($productos);$i++)
        {
          $pc= new ProductoCompra(null,$id,$productos[$i]);

          $p->setIdProducto($productos[$i]);

          $pc->setPrecioCompra($precios[$i]);
          $pc->setCantidad($cantidades[$i]);
          $pc->guardar();

          $nuevacant=armarArrayCon($p->traerProducto())[0]['cantidadactual']*1+$cantidades[$i];
          $p->setCantidadProducto($nuevacant);
          $p->setPrecioCompraProducto($precios[$i]);
          $p->actualizarCantidad();
          $p->actualizarPrecioCompra();
        }
        break;
  


  //--------------------------------------------------------------------------------------------------- Productos ------------------

    case "guardarProductos":
          require_once("Clases/Logica/Producto.php");
          require_once("Clases/Logica/Accion.php");

            $arrayNombre = json_decode($_POST['arrayNombre']);
            $arrayTipoVenta = json_decode($_POST['arrayTipoVenta']);
            $arrayPrecioCompra = json_decode($_POST['arrayPrecioCompra']);
            $arrayporcentajeVenta = json_decode($_POST['arrayporcentajeVenta']);
            $arrayCantidad = json_decode($_POST['arrayCantidad']);
            $arrayPrecio = json_decode($_POST['arrayPrecio']);

            for($i = 0; $i<sizeof($arrayNombre) ; $i++){
              $td = new Producto();
              $td->setNombreProducto($arrayNombre[$i]);
              $td->setTipoVenta($arrayTipoVenta[$i]);
              $td->setCantidadProducto($arrayCantidad[$i]);
              $td->setPrecioProducto($arrayPrecio[$i]);
              $td->setPrecioCompraProducto($arrayPrecioCompra[$i]);
              $td->setComisionProducto($arrayporcentajeVenta[$i]);

              $td->guardar();
                                //--- Guardo el reporte de ingreso al sistema
                  $a = new Accion();
                  $a->setIdUsuario($_SESSION['id']);
                  $accion = "El usuario agregó el producto ". $arrayNombre[$i] . " al sistema." ;
                  $a->setAccion($accion);
                  $a->setFechaHora(date("d") . "/" . date("m") . "/" . date("Y") . "  -  " . date("H") . ":" . date("i"));
                  $a->guardar();
            }
          break;



    case "traerTodosProductos":
          require_once("Clases/Logica/Producto.php");
          $productos = Producto::traerTodos();
          $productos = armarArrayCon($productos);
          if (isset($_POST['idProveedor'])){              
              $idProductoProveedor = Producto::traerTodosPorProveedor($_POST['idProveedor']);
              $productos[0]['idProductoProveedor'] = armarArrayCon($idProductoProveedor);
          }
          $respuesta = array('err'=>0, 'productos'=>$productos);
          break;


    case "eliminarProducto":
        require_once('Clases/Logica/Producto.php');
        require_once("Clases/Logica/Accion.php");

        $p=new Producto();

                //--- Guardo el reporte de ingreso al sistema
                  $a = new Accion();
                  $a->setIdUsuario($_SESSION['id']);
                  $nombre=armarArrayCon($p->getNombreXid($_POST['idProducto']));

                  $accion = "El usuario elimino el producto ". $nombre[0]['nombre'] . " del sistema." ;
                  $a->setAccion($accion);
                  $a->setFechaHora(date("d") . "/" . date("m") . "/" . date("Y") . "  -  " . date("H") . ":" . date("i"));
                  $a->guardar();

        $p->eliminar();
        $respuesta=array('err'=>0);
        break;
    case "traerProducto":
        require_once("Clases/Logica/Producto.php");
        $p=new Producto();
        $rta=$p->traerProducto();
        $respuesta=array('err'=>0, 'producto'=>armarArrayCon($rta));
        break;


  //--------------------------------------------------------------------------------------------- Usuarios ----------------------------
    case "traerUsuarioActual":
            require_once("Clases/Logica/Usuario.php");
            $t = new Usuario();
            $t->setIdUsuario($_SESSION['id']);
            $rta = $t->traerUsuario();
            $respuesta=array('err'=>0, 'usuario'=>armarArrayCon($rta));
            break;

    case "guardarUsuario":
            require_once("Clases/Logica/Usuario.php");
            require_once("Clases/Logica/Accion.php");

            $t = new Usuario();
            $t->guardar();
                                //--- Guardo el reporte de ingreso al sistema
                  $a = new Accion();
                  $a->setIdUsuario($_SESSION['id']);
                  $accion = "El usuario agrego el usuario ". $t->getTipoUsuario(). " ". $_POST['nombreUsuario'] . ", ". $_POST['apellidoUsuario'] ." al sistema." ;
                  $a->setAccion($accion);
                  $a->setFechaHora(date("d") . "/" . date("m") . "/" . date("Y") . "  -  " . date("H") . ":" . date("i"));
                  $a->guardar();

            break;

    case "guardarObservacion":
            require_once("Clases/Logica/Usuario.php");
            $t= new Usuario();
            $t->actualizarObservacion($_POST['observacion']);

            break;

    case "traerTodosUsuarios":
            require_once("Clases/Logica/Usuario.php");
            require_once("Clases/Logica/Accion.php");
            $usuarios = Usuario::traerTodos();
            //$acciones = Accion::traerTodos();
            $usuarios = armarArrayCon($usuarios);
        //-------------------- Tomo la fecha y hora del ultimo acceso al sistema del usuario
            for($i = 0; $i < sizeof($usuarios); $i++){
                $acciones = Accion::traerAccesos($usuarios[$i]['id']);
                $usuarios[$i]['acciones'] = armarArrayCon($acciones);
                if(sizeof($usuarios[$i]['acciones']) == 0){
                    $usuarios[$i]['acciones'][0]['fechahora'] = "El usuario nunca ingreso al sistema";
                }
            }
           //---- Me fijo si el usuario nunca ingreso

        

            $respuesta= array('err'=>0, 'usuarios'=>$usuarios);
            break;

    case 'traerUsuario':
            require_once("Clases/Logica/Usuario.php");
            $usuario = new Usuario();
            $rta = $usuario->traerUsuario();
            $cajas=$usuario->traerCajas();
            $respuesta = array('err'=>0, 'usuario'=>armarArrayCon($rta), 'cajas'=>armarArrayCon($cajas));
            break;

    case 'traerUsuarioAdministrador':
            require_once("Clases/Logica/Usuario.php");
            require_once("Clases/Logica/Retiro.php");
            require_once("Clases/Logica/Accion.php");
            require_once("Clases/Logica/Proveedor.php");
            require_once("Clases/Logica/Caja.php");
            require_once("Clases/Logica/Compra.php");

            $u = new Usuario();
            $p = new Proveedor();
            $rta=$u->traerUsuario();
            $usuario = armarArrayCon($rta);
     
            
      //---- Obtengo las compras que realizo el usuario
            $usuario[0]['compras'] = armarArrayCon(Compra::comprasXusuario($usuario[0]['id']));
            
            for($i = 0; $i < sizeof($usuario[0]['compras']); $i++){
                $usuario[0]['compras'][$i]['proveedor'] = armarArrayCon($p->traerProveedor( $usuario[0]['compras'][$i]['idproveedores']));
            }
            
             //---- Obtengo los retiros que realizo el usuario
            $usuario[0]['retiros'] = armarArrayCon(Retiro::retirosXusuario($usuario[0]['id']));
          //---- Obtengo el usuario vendedor al que se le realizo el retiro
            for($i = 0; $i < sizeof($usuario[0]['retiros']); $i++){
                $vendedor = Caja::obtenerVendedor($usuario[0]['retiros'][$i]['cajas_id']); 
                $usuario[0]['vendedor'] = armarArrayCon($vendedor);
            }

      //----- Obtengo los reportes del usuario
            $usuario[0]['reportes'] = armarArrayCon(Accion::accionesXusuario($usuario[0]['id']));



            $respuesta = array('err'=>0, 'usuario'=> $usuario);
            break;

    case "bloquearUsuario":
            require_once("Clases/Logica/Usuario.php");
            require_once("Clases/Logica/Accion.php");

            $u = new Usuario();
                                //--- Guardo el reporte de ingreso al sistema
                  $a = new Accion();
                  $a->setIdUsuario($_SESSION['id']);
                  //$tipoUsuario = armarArrayCon();
                  $nombre = armarArrayCon($u->getNombreXid($_POST['idUsuario']));
                  $apellido = armarArrayCon($u->getApellidoXid($_POST['idUsuario']));
                  $accion = "El usuario bloqueo el usuario ". $nombre[0]['nombre'] . ", ". $apellido[0]['apellido'] ." al sistema." ;
                  $a->setAccion($accion);
                  $a->setFechaHora(date("d") . "/" . date("m") . "/" . date("Y") . "  -  " . date("H") . ":" . date("i"));
                  $a->guardar();
            $u->bloquear();
            break;

    case "desbloquearUsuario":
            require_once("Clases/Logica/Usuario.php");
            require_once("Clases/Logica/Accion.php");
            
            $u= new Usuario();
                                //--- Guardo el reporte de ingreso al sistema
                  $a = new Accion();
                  $a->setIdUsuario($_SESSION['id']);
                  $nombre = armarArrayCon($u->getNombreXid($_POST['idUsuario']));
                  $apellido = armarArrayCon($u->getApellidoXid($_POST['idUsuario']));
                  $accion = "El usuario desbloqueo el usuario ". $nombre[0]['nombre'] . ", ". $apellido[0]['apellido'] ." al sistema." ;
                  $a->setAccion($accion);
                  $a->setFechaHora(date("d") . "/" . date("m") . "/" . date("Y") . "  -  " . date("H") . ":" . date("i"));
                  $a->guardar();
            $u->desbloquear();
            break;

    //case "traerCajasSegunUsuario":

  //--------------------------------------------------------------------------------------------- Proveedores ----------------------------------
    case "traerTodosProveedores":
          require_once("Clases/Logica/Proveedor.php");
          $proveedores = Proveedor::traerTodos();
          $respuesta = array('err'=>0, 'proveedores'=>armarArrayCon($proveedores));
          break;

    case "guardarProveedor":
          require_once("Clases/Logica/Proveedor.php");
          $p = new Proveedor();
          $p->guardar();
          break;
    case 'traerProveedor':
            require_once("Clases/Logica/Proveedor.php");
            $proveedor = new Proveedor();
            $rta = $proveedor->traerProveedor($_POST["idProveedor"]);            
            $respuesta = array('err'=>0, 'proveedor'=>armarArrayCon($rta));
            break;
    case "eliminarProveedor":
          require_once("Clases/Logica/Proveedor.php");
          $p = new Proveedor();
          $p->eliminar();
          break;
    case "añadirProductosProveedor":
          require_once("Clases/Logica/Proveedor.php");
          $productos=$_REQUEST['productos'];
          ConexionBD::obtenerInstancia()->consultar("delete FROM productosproveedores  WHERE idproveedor = $_REQUEST[idProveedor]");
          if(sizeof($productos)>0)
          {
            $productosExisten=armarArrayCon(ConexionBD::obtenerInstancia()->consultar("SELECT idproducto FROM productosproveedores WHERE idproveedor = $_REQUEST[idProveedor]"));
            $sql="INSERT INTO productosproveedores (id, idproveedor, idproducto) VALUES ";
            for($i=0; $i<sizeof($productos); $i++)
            {
              if(!in_array(['idproducto'=>$productos[$i]], $productosExisten))
                $sql.="(NULL, $_REQUEST[idProveedor], $productos[$i]),";
            }

            $sql=substr($sql, 0, -1);
            if(strlen($sql)>strlen('INSERT INTO productosproveedores (id, idproveedor, idproducto) VALUES'))  //si hay algún producto para insertar...
              ConexionBD::obtenerInstancia()->consultar($sql);
          }
          $respuesta = array('err'=>0);
          break;

          
  //-----------------------------------------------------------------------------------------Reportes/Resumenes---------------------------------
    case "traerTodosRegistros":
            require_once("Clases/Logica/Registro.php");
            $registros=Registro::traerTodos();
            $respuesta=array('err'=>0, 'registros'=>armarArrayCon($registros));
            break;

    case "traerTodosCajas":
            require_once("Clases/Logica/Caja.php");
            $registros=Caja::traerTodos();
            $respuesta=array('err'=>0, 'cajas'=>armarArrayCon($registros));
            break;

    case "mostrarCaja":
            require_once("Clases/Logica/Caja.php");
            require_once("Clases/Logica/Venta.php");
            require_once("Clases/Logica/Usuario.php");
            $idCaja=$_REQUEST['idCaja'];
            /////////Traigo datos de la caja
            $datosCaja=armarArrayCon(Caja::traerCaja($idCaja)); //VER SI ESTO CONVIENE ASI O TRABAJAR CON EL CONSTRUCTOR DE CAJA Y LOS $_POST
            //////////Traigo datos del usuario de esa caja
            $usuario=new Usuario();
            $usuario->setIdUsuario(($datosCaja[0]['idusuario'])); 
            $u=$usuario->traerUsuario();
            //Traigo los retiros para esa caja
            $retiros=armarArrayCon(Caja::traerRetirosDeCaja($idCaja));
            //Traigo el usuario del retiro para mostrarlo tambien
            for($i=0; $i<sizeof($retiros); $i++)
            {
              $usuario=new Usuario();
              $usuario->setIdUsuario($retiros[$i]['usuarios_id']);
              $retiros[$i]['usuario']=armarArrayCon($usuario->traerUsuario());
            }
            //Traigo las ventas para esa caja
            $ventas=armarArrayCon(Caja::traerVentasDeCaja($idCaja));
            //Para cada venta, traigo los productos vendidos
            for($i=0; $i<count($ventas);$i++)
            {
              $ventas[$i]['productos']=armarArrayCon(Venta::traerProductosDeVenta($ventas[$i]['id']));
            }
            //Armo la respuesta json
            $respuesta=array('err'=>0, 'caja'=>$datosCaja, 'usuario'=>armarArrayCon($u), 'retiros'=>$retiros, 'ventas'=>$ventas);
            break;

    case "mostrarCompra":
            require_once("Clases/Logica/Compra.php");
            require_once("Clases/Logica/Proveedor.php");
            require_once("Clases/Logica/Usuario.php");
            require_once("Clases/Logica/Producto.php");
            require_once("Clases/Logica/ProductoCompra.php");

            $idCompra=$_REQUEST['idCompra'];
            /////////Traigo datos de la compra
            $datosCompra=armarArrayCon(Compra::traerCompra($idCompra)); 

            //////////Traigo datos del usuario de esa compra
            $usuario=new Usuario();
            $usuario->setIdUsuario(($datosCompra[0]['idusuarios'])); 
            $u=$usuario->traerUsuario();

            //////////Traigo datos del proveedor de esa compra
            $proveedor=new Proveedor(); 
            $p=$proveedor->traerProveedor($datosCompra[0]['idproveedores']);

            //Traigo los productos de esa compra
            $productos=armarArrayCon(Compra::traerProductosDeCompra($idCompra));

            //Traigo la informacion de los productos
            for($i=0; $i<sizeof($productos); $i++)
            {
              $producto=new Producto();
              $producto->setIdProducto($productos[$i]['idproducto']);
              $productos[$i]['producto']=armarArrayCon($producto->traerProducto());
            }
            //Traigo las ventas para esa caja
            //$ventas=armarArrayCon(Caja::traerVentasDeCaja($idCaja));
            //Para cada venta, traigo los productos vendidos
           /* for($i=0; $i<count($ventas);$i++)
            {
              $ventas[$i]['productos']=armarArrayCon(Venta::traerProductosDeVenta($ventas[$i]['id']));
            }*/
            //Armo la respuesta json
            $respuesta=array('err'=>0, 'compra'=>$datosCompra, 'usuario'=>armarArrayCon($u),'proveedor'=>armarArrayCon($p), 'productos'=>$productos);
            break;
}

echo json_encode($respuesta);

function armarArrayCon($resDb){
        $array=array();
        if($resDb!=false){  //CONDICION PARA QUE NO EXPLOTE SI NO ENCUENTRA REGISTROS
              while($datos  = $resDb->fetch_assoc()){
                      $array[] = $datos;
              }
        }
        return $array;
}
