function checkUser(){
$.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"checkUser"},
    url: "./api.php", success: function(datos,texto,jqXHR){

        if(datos.err==1)
        	location.href = 'index.html';
        else if(datos.permiso=='Vendedor')
        {
          if(datos.existeCaja==0)
          {
            $("#mainPanel").load("vista/terminalVenta/montoInicial.html");
            $("#menuMain").load("vista/terminalVenta/menuTerminal.html");
            $("#mainTitulo").html("Terminal de Ventas - CEFI Administración");
          }
          else
          {
            $("#mainPanel").load("vista/terminalVenta/terminalVenta.html");
            $("#menuMain").load("vista/terminalVenta/menuTerminal.html");
            $("#mainTitulo").html("Terminal de Ventas - CEFI Administración");
          }
        }	
        else if(datos.permiso=='Administrador')
        {
            $("#mainPanel").load("vista/terminalAdministrador/terminalAdministrador.html");
            $("#menuMain").load("vista/terminalAdministrador/menuAdministrador.html");
        }

      }
  });
}
