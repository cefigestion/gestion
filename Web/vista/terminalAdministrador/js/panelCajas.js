function mostrarCaja(id){
	$.ajax({  //traigo los datos de la caja
                    type: "POST",cache: false,   dataType:'json', data: {"t":"mostrarCaja", "idCaja":id},
                    url: "./api.php", 
                    success: function(datos,texto,jqXHR){
                    	////////////////////////////Datos de la caja
                    	var html='<div class="row" style="background-color:white"><div class="col-md-6"><br><b>Fecha de Apertura: </b>'+formatoFecha(datos.caja[0].fechahoraapertura)+'<br><br>';
                    	html=html+'<b>Fecha de Cierre: </b>'+formatoFecha(datos.caja[0].fechahoracierre)+'<br><br>';
                    	html=html+'<b>Monto Inicial: </b>'+datos.caja[0].montoinicial+' <b>Monto Final: </b>'+datos.caja[0].montofinal+'<br><br></div>';

                    	/////////////////////////Datos del usuario
                    	html+='<div class="col-md-6"><h4><b>Usuario responsable:</b></h4><b>Nombre y Apellido: </b>';
                    	html=html+datos.usuario[0].nombre+' '+datos.usuario[0].apellido+'<br>';
                    	html=html+'<b>DNI: </b>'+datos.usuario[0].dni+'<br>';
                    	html+='<b>Tipo de usuario: </b>';
                    	html+=(datos.usuario[0].tipo==1)?'Vendedor':'Administrador';
                    	html+='</div></div>';
                    	$("#contenedorDatosCaja").html(html);
 
                    	/////////////////////////Retiros de Caja
                    	html='<table class="table table-hover" style="background-color:white"><thead><tr><th>Fecha de Retiro</th><th>Monto</th><th>Responsable</th></tr></thead><tbody>';
                    	for(var i=0; i<datos.retiros.length;i++)
                    	{
                    		html+='<tr><td>';
                    		html+=formatoFecha(datos.retiros[i].fecha);
                    		html+='</td><td>';
                    		html+=datos.retiros[i].monto;
                    		html+='</td><td>';
                    		html=html+datos.retiros[i].usuario[0].nombre+' '+datos.retiros[i].usuario[0].apellido+'</td></tr>';
                    	}
                    	html+='</tbody></table>';
                    	$("#contenedorRetiros").html(html);


                    	////////////////////////////Ventas de Caja
                    	html='<div class="panel-group">';
                    	for(var i=0; i< datos.ventas.length; i++)
                    	{
                    		html+='<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title">';
                    		html=html+'Fecha de Venta: '+formatoFecha(datos.ventas[i].fecha)+' '+datos.ventas[i].hora+' Cancelada: ';
                    		html+=(datos.ventas[i].cancelar==0)?'NO':'SI';
                    		html=html+' '+'Monto Total: '+datos.ventas[i].total+' ';
                    		html=html+'<a data-toggle="collapse" href="#venta'+datos.ventas[i].id+'"><b>Ver</b></a></h4></div>';
                    		html=html+'<div id="venta'+datos.ventas[i].id+'" class="panel-collapse collapse"><ul class="list-group">';
                    		for(var j=0; j<datos.ventas[i].productos.length; j++)
                    		{
                    			html+='<li class="list-group-item">';
                    			html=html+datos.ventas[i].productos[j].nombre+' Cantidad: '+datos.ventas[i].productos[j].cantidad+' Precio: '+datos.ventas[i].productos[j].precio+'</li>';
                    		}
                    		html+='</ul></div></div>';
                    	}
                    	html+='</div>';
                    	$("#contenedorVentas").html(html);

                    	$("#modalMostrarCaja").modal("show");
                    }
            });
}


////////////////////////////Buscador de reportes, por fecha
function cargarResultadoReportes(){
     var str=$('#busBuscarReporte').val();
     console.log(str);
     if(str!="")
     {
          stringfecha=formatoFecha(str);
          var filas=document.getElementsByName("filaReporte"); //filas de las dos tablas, reportes y cajas
          for(var i=0; i<filas.length; i++)
          {
               fechaFila=filas[i].childNodes[0].innerText;
               if(fechaFila.search(stringfecha)<0)
               {
                    filas[i].style.display="none";
               }
               else
               {
                    filas[i].style.display="";
               }
          }
     }
     else
     {
          var filas=document.getElementsByName("filaReporte"); //filas de las dos tablas, reportes y cajas
          for(var i=0; i<filas.length; i++)
          {
               filas[i].style.display="";
          }    //Esto es para que vuelvan a ser visibles si no busca nada
     }

}