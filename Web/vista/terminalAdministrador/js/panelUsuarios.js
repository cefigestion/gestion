$.fn.initPanelUsuarios = function () {

  $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"traerTodosUsuarios"},
    url: "./api.php", success: function(datos,texto,jqXHR){
      $("#panelBusqueda").empty();
      $("#paginationContenedor").empty();
      $("#panelBusqueda").append(
          '<div class="col-lg-8" style="margin-left: 0%"><div class="input-group"><input type="text" class="form-control" id="busBuscarUsuario" placeholder="Usuario...." onkeyup="cargarResultadoXclase(\'busBuscarUsuario\',\'resultadoUsuarios\')"><span class="input-group-btn"><button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalNuevoUsuario" style="background-color: #2980B9; color: #ECF0F1" >Buscar</button></span></div></div><div class="col-lg-4"><button class="btn btn-block btn-primary" name="botonNuevoUsuario"  id="botonNuevoUsuario"  onclick=vaciarFormularios("usuarios") data-toggle="modal" data-target="#modalNuevoUsuario" style="background-color: #3498DB; border-color: #3498DB; color: #ECF0F1;"><span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 5%"></span><span class="glyphicon glyphicon-open-file" aria-hidden="true"></span></button></div>'
      );

      $("#contenedorVendedores").empty();
      $("#contenedorAdministradores").empty();
      //Accedemos a la informacion como datos.pacientes[<numero>].<atributo>
      for (var i = 0; i < datos.usuarios.length; i++) {

          if(datos.usuarios[i].bloqueado==0){
            // Vista para usuario no bloqueado
            var popUpEliminar='swal({title: "¿Estas seguro?",text: "Un usuario bloqueado no podra acceder al sistema.",type: "warning",showCancelButton: true,confirmButtonClass: "btn-danger",confirmButtonText: "Si, quiero bloquearlo!",closeOnConfirm: false},function(){swal("Bloqueado!", "El usuario ha sido bloqueado correctamente.", "success");bloquearUsuario('+datos.usuarios[i].id+')});';
            var botonEliminar='<button type="button" onclick=\''+popUpEliminar+'\' class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB">Bloquear</button>';
            var botonModificar = '<button type="button" onclick="modificarUsuario('+datos.usuarios[i].id+')" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #2980B9" data-toggle="modal" data-target="#modalNuevoUsuario">Modificar</button>';
            
            //--- Filtor por administradores y vendedores ----
            if(datos.usuarios[i].tipo == 0){
               var botonVer= '<button onclick="verUsuario('+datos.usuarios[i].id+', \'Administrador\')" type="button" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB">Ver</button>';
              $("#contenedorAdministradores").append(
                  '<li name="resultadoUsuarios" class="list-group-item row"><div class="col-md-8" style="padding-left: 2%; padding-right:2%"><p name=\''+datos.usuarios[i].apellido+'\' id=\''+datos.usuarios[i].apellido+',' +datos.usuarios[i].apellido + ','+datos.usuarios[i].nombre+'\'><b>Nombre:</b>  ' +datos.usuarios[i].apellido + ', ' + datos.usuarios[i].nombre + '</p></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Ultimo Acceso:</b> '+ datos.usuarios[i]['acciones'][0].fechahora +' </p></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Estado:</b> Activo</p></div>'+botonVer+botonModificar+botonEliminar+'</li>' 
              );
            }
            else{
              var botonVer= '<button onclick="verUsuario('+datos.usuarios[i].id+', \'Vendedor\')" type="button" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB">Ver</button>';
              $("#contenedorVendedores").append(
                  '<li name="resultadoUsuarios" class="list-group-item row"><div class="col-md-8" style="padding-left: 2%; padding-right:2%"><p name=\''+datos.usuarios[i].apellido+'\' id=\''+datos.usuarios[i].apellido+',' +datos.usuarios[i].apellido + ','+datos.usuarios[i].nombre+'\'><b>Nombre:</b>  ' +datos.usuarios[i].apellido + ', ' + datos.usuarios[i].nombre + '</p></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Ultimo Acceso:</b> '+ datos.usuarios[i]['acciones'][0].fechahora +' </p></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Estado:</b> Activo</p></div>'+botonVer+botonModificar+botonEliminar+'</li>' 
      
              );
            }
          }else{
            // Vista para usuario bloqueado
            var popUpEliminar='swal({title: "¿Estas seguro?",text: "Un usuario desbloqueado podrá acceder al sistema.",type: "warning",showCancelButton: true,confirmButtonClass: "btn-danger",confirmButtonText: "Si, quiero desbloquearlo!",closeOnConfirm: false},function(){swal("Desbloqueado!", "El usuario ha sido desbloqueado correctamente.", "success");desbloquearUsuario('+datos.usuarios[i].id+')});';
            var botonEliminar='<button type="button" onclick=\''+popUpEliminar+'\' class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB">Desbloquear</button>';
            var botonModificar = '<button type="button" onclick="modificarUsuario('+datos.usuarios[i].id+')" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #2980B9">Modificar</button>';
            
            //--- Filtor por administradores y vendedores ----
            if(datos.usuarios[i].tipo == 0){
              $("#observacion").empty();
              $("#botonGuardar").empty();
             // document.getElementById('observacion').innerHTML='';
              //document.getElementById('botonGuardar').innerHTML='';
              var botonVer= '<button onclick="verUsuario('+datos.usuarios[i].id+', \'Administrador\')" type="button" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB">Ver</button>';
              $("#contenedorAdministradores").append(
                  '<li name="resultadoUsuarios" class="list-group-item row"><div class="col-md-8" style="padding-left: 2%; padding-right:2%"><p name=\''+datos.usuarios[i].apellido+'\' id=\''+datos.usuarios[i].apellido+','+datos.usuarios[i].nombre+',BLOQUEADO\'><b>Nombre:</b>  ' +datos.usuarios[i].apellido + ', ' + datos.usuarios[i].nombre + '</p></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Ultimo Acceso:</b> '+ datos.usuarios[i]['acciones'][0].fechahora +' </p></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Estado:</b> Bloqueado</p></div>'+botonVer+botonModificar+botonEliminar+'</li>' 
              );
            }
            else{

              var botonVer= '<button onclick="verUsuario('+datos.usuarios[i].id+', \'Vendedor\')" type="button" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB">Ver</button>';
              $("#contenedorVendedores").append(
                  '<li name="resultadoUsuarios" class="list-group-item row"><div class="col-md-8" style="padding-left: 2%; padding-right:2%"><p name=\''+datos.usuarios[i].apellido+'\' id=\''+datos.usuarios[i].apellido+','+datos.usuarios[i].nombre+',BLOQUEADO\'><b>Nombre:</b>  ' +datos.usuarios[i].apellido + ', ' + datos.usuarios[i].nombre + '</p></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Ultimo Acceso:</b> '+ datos.usuarios[i]['acciones'][0].fechahora +'  </p></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Estado:</b> Bloqueado</p></div>'+botonVer+botonModificar+botonEliminar+'</li>' 
              );
            }                  

          }
      } 
    }
  });
};





//--------------------- Funciones de usuarios -------------------------
function verUsuario(id, tipo){
  if(tipo == 'Administrador'){
                $.ajax({
                  type: "POST",cache: false,   dataType:'json', data: {"t":"traerUsuarioAdministrador","idUsuario":id},
                  url: "./api.php", success: function(datos,texto,jqXHR){
                          $(document).ready(function(){
                                  
                                  $("#tituloModalUsuariosAdm").text("Información del usuario");
                                  //-------------------------
                                  $("#idUsuarioA").html(datos.usuario[0].id);
                                  $("#nombreUsuarioA").html(datos.usuario[0].nombre);
                                  $("#apellidoUsuarioA").html(datos.usuario[0].apellido);
                                  $("#dniUsuarioA").html(datos.usuario[0].dni);

                                  if(datos.usuario[0].bloqueado == 0)
                                      $("#estadoUsuarioA").html("Activo");
                                  else
                                      $("#estadoUsuarioA").html("Bloqueado");

                                    $("#tipoUsuarioA").html("Administrador");
                                  
        //------------------------------------------------------------------ Pagination de las compras del usuario
                                  var htmlPagination = '';
                                  var html= '<div id="compras0" class="tab-pane fade col-md-12 active in"><ul class="list-group"><ul>';
                              //------ Me fijo si voy a necesitar la navegacion lateral
                                  if(datos.usuario[0]['compras'].length > 45){
                                      htmlPagination += '<li id="previous"><a href="#" onclick="cambiarPagination(\'previous\',\'paginaCompras\',\'linkCompras\',\'resultadosCompra\')" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                                      htmlPagination += '<li id="paginaCompras0" class="paginaCompras"><a id="linkCompras0" data-toggle="tab" aria-expanded="true" href="#compras0">1</a></li>';
                                  }else{
                                      htmlPagination += '<li><a id="linkCompras0" data-toggle="tab" aria-expanded="true" href="#compras0">1</a></li>';
                                  }
                                  // --- Creo una lista cada 5 reportes                          

                                  for(var i=0; i<datos.usuario[0]['compras'].length; i++ ){
                                              //--- Si pasan 5 reportes creo una lista
                                              if((i%5 == 0)&&(i!=0)){
                                                //--- Agrego una pagina nueva en el pagination
                                                if((datos.usuario[0]['compras'].length>45)&&(i == 40)&&(i != datos.usuario[0]['compras'].length -1)){                                      
                                                     htmlPagination += '<li><a href="#">...</a></li>';
                                                }
                                                //console.log("I: " + Math.ceil($i/5) + "   total: " + Math.ceil(datos.usuario[0]['reportes'].length/5-1));
                                                if((i>=40)&&(Math.ceil(i/5)!=Math.ceil(datos.usuario[0]['compras'].length/5-1))){
                                                 // console.log($i);
                                                    htmlPagination += '<li id="paginaCompras'+Math.trunc(i/5)+'" class="hidden paginaCompras"><a id="linkCompras'+Math.trunc(i/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination(\'next\',\'paginaCompras\',\'linkCompras\',\'resultadosCompra\')">'+Math.trunc(i/5 +1) +'</a></li>';
                                                }else{                                       
                                                    htmlPagination += '<li id="paginaCompras'+Math.trunc(i/5)+'" class="paginaCompras"><a id="linkCompras'+Math.trunc(i/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination(\'next\',\'paginaCompras\',\'linkCompras\',\'resultadosCompra\')">'+Math.trunc(i/5 +1) +'</a></li>';              
                                                }      
                                               
                                                //-- Agrego la lista al div contenedor                                                
                                                $("#listaCompras").html(html);                                               
                                              }
                                              
                                            // Muestro la informacion basica de la compra VER BIEN COMO SE GUARDA EN LA BD
                                            if(Math.trunc(i/5)==0)                                                
                                              html+= '<li name="resultadosCompra" data-toggle="collapse" style="display: block;" data-parent="#compras'+Math.trunc(i/5)+'" name="" class="resultadosCompra'+Math.trunc(i/5)+' list-group-item row"><div data-toggle="collapse" data-parent="#cajas'+Math.trunc(i/5)+'" class="col-md-8" ><p><b>Fecha compra:</b>  ' +formatoFecha(datos.usuario[0]['compras'][i].fecha) + '</p></div><div class="col-md-10"><div class="col-md-5"><p><b>Proveedor:</b>'+datos.usuario[0]['compras'][i]['proveedor'][0].razonsocial +'</p></div><div class="col-md-5"><p><b>Cantidad:</b>'+datos.usuario[0]['compras'][i].cantidad+'</p></div></div><a onclick="javascript:mostrarCompra('+datos.usuario[0]['compras'][i].id+');">Ver Compra</a></li>';                                               
                                            else
                                              html+= '<li name="resultadosCompra" data-toggle="collapse" style="display: none;" data-parent="#compras'+Math.trunc(i/5)+'" name="" class="resultadosCompra'+Math.trunc(i/5)+' list-group-item row"><div data-toggle="collapse" data-parent="#cajas'+Math.trunc(i/5)+'" class="col-md-8" ><p><b>Fecha compra:</b>  ' +formatoFecha(datos.usuario[0]['compras'][i].fecha) + '</p></div><div class="col-md-10"><div class="col-md-5"><p><b>Proveedor:</b>'+datos.usuario[0]['compras'][i]['proveedor'][0].razonsocial +'</p></div><div class="col-md-5"><p><b>Cantidad:</b>'+datos.usuario[0]['compras'][i].cantidad+'</p></div></div><a onclick="javascript:mostrarCompra('+datos.usuario[0]['compras'][i].id+');">Ver Compra</a></li>';                                               
                                    
                                  }


                                   console.log("Cantidad de comprar "+datos.usuario[0]['compras'].length);

                                  if(datos.usuario[0]['compras'].length > 45){
                                    htmlPagination += '<li><a href="#" onclick="cambiarPagination(\'next\',\'paginaCompras\',\'linkCompras\',\'resultadosCompra\')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                                  }

                                  if(datos.usuario[0]['compras'].length == 0){
                                      html = '<ul><li data-toggle="collapse" data-parent="#listaCompras" name="" class="list-group-item row"><h3>No se encontraron resultados</h3></li>';
                                      html += '</ul></div>';
                                  }

                                  $("#paginationCompras").html(htmlPagination);
                                  $("#listaCompras").html(html);
                                  //-- Selecciono la primer pagina del pagination, porque si no 
                                  $("#linkCompras0").click();





        //------------------------------------------------------------------ Pagination de los retiros del usuario
                                  var htmlPagination = '';
                                  var html= '<div id="retiros0" class="tab-pane fade col-md-12 active in"><ul class="list-group"><ul>';
                              //------ Me fijo si voy a necesitar la navegacion lateral
                                  if(datos.usuario[0]['retiros'].length > 45){
                                      htmlPagination += '<li id="previous"><a href="#" onclick="cambiarPagination(\'previous\',\'paginaRetiros\',\'linkRetiros\',\'resultadoRetiros\')" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                                      htmlPagination += '<li id="paginaRetiros0" class="paginaRetiros"><a id="linkRetiros0" data-toggle="tab" aria-expanded="true" href="#retiros0">1</a></li>';
                                  }else{
                                      htmlPagination += '<li><a id="linkRetiros0" data-toggle="tab" aria-expanded="true" href="#retiros0">1</a></li>';
                                  }
                                  // --- Creo una lista cada 5 reportes
                                  for(var $i=0; $i<datos.usuario[0]['retiros'].length; $i++ ){
                                              //--- Si pasan 5 reportes creo una lista
                                              if(($i%5 == 0)&&($i!=0)){
                                                //--- Agrego una pagina nueva en el pagination
                                                if((datos.usuario[0]['retiros'].length > 45)&&($i == 40)&&($i != datos.usuario[0]['retiros'].length -1)){                                      
                                                     htmlPagination += '<li><a href="#">...</a></li>';
                                                }
                                                //console.log("I: " + Math.ceil($i/5) + "   total: " + Math.ceil(datos.usuario[0]['reportes'].length/5-1));
                                                if(($i>=40)&&(Math.ceil($i/5)!=Math.ceil(datos.usuario[0]['retiros'].length/5-1))){
                                                 // console.log($i);
                                                    htmlPagination += '<li id="paginaRetiros'+Math.trunc($i/5)+'" class="hidden paginaRetiros"><a id="linkRetiros'+Math.trunc($i/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination(\'previous\',\'paginaRetiros\',\'linkRetiros\',\'resultadoRetiros\')">'+Math.trunc($i/5 +1) +'</a></li>';
                                                }else{                                        
                                                    htmlPagination += '<li id="paginaRetiros'+Math.trunc($i/5)+'" class="paginaRetiros"><a id="linkRetiros'+Math.trunc($i/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination(\'previous\',\'paginaRetiros\',\'linkRetiros\',\'resultadoRetiros\')">'+Math.trunc($i/5 +1) +'</a></li>';              
                                                }      
                                                
                                                //-- Agrego la lista al div contenedor
                                                $("#listaRetiros").html(html);
                                               // html += '<div id="retiros'+Math.trunc($i/5)+'" class="tab-pane fade col-md-12 active in"><ul class="list-group"><ul>';
                                              }
                                              
                                              if(Math.trunc($i/5)==0)                                            
                                                html+= '<li name="resultadoRetiros" data-toggle="collapse" style="display: block;" data-parent="#retiros'+Math.trunc($i/5)+'" class="resultadoRetiros'+Math.trunc($i/5)+' list-group-item row"><div data-toggle="collapse" data-parent="#retiros'+Math.trunc($i/5)+'" class="col-md-8" ><p><b>Fecha:</b>  ' +datos.usuario[0]['retiros'][$i].fecha + '</p></div><div data-toggle="collapse" data-parent="#listaRetiros" class="col-md-8" style=" ><p><b>Monto:</b> '+datos.usuario[0]['retiros'][$i].monto+'</p></div><div data-toggle="collapse" data-parent="#listaRetiros" class="col-md-8" ><p><b>Vendedor:</b>  ' +datos.usuario[0]['vendedor'][0].apellido + ', '+ datos.usuario[0]['vendedor'][0].nombre +'</p></div></li>'; 
                                              else
                                                html+= '<li name="resultadoRetiros" data-toggle="collapse" style="display: none;" data-parent="#retiros'+Math.trunc($i/5)+'" class="resultadoRetiros'+Math.trunc($i/5)+' list-group-item row"><div data-toggle="collapse" data-parent="#retiros'+Math.trunc($i/5)+'" class="col-md-8" ><p><b>Fecha:</b>  ' +datos.usuario[0]['retiros'][$i].fecha + '</p></div><div data-toggle="collapse" data-parent="#listaRetiros" class="col-md-8" style=" ><p><b>Monto:</b> '+datos.usuario[0]['retiros'][$i].monto+'</p></div><div data-toggle="collapse" data-parent="#listaRetiros" class="col-md-8" ><p><b>Vendedor:</b>  ' +datos.usuario[0]['vendedor'][0].apellido + ', '+ datos.usuario[0]['vendedor'][0].nombre +'</p></div></li>'; 
                                    
                                  }
                                  if(datos.usuario[0]['retiros'].length > 45){
                                    htmlPagination += '<li><a href="#" onclick="cambiarPagination(\'previous\',\'paginaRetiros\',\'linkRetiros\',\'resultadoRetiros\')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                                  }

                                  if(datos.usuario[0]['retiros'].length == 0){
                                      html = '<ul><li data-toggle="collapse" data-parent="#listaRetiros" name="" class="list-group-item row"><h3>No se encontraron resultados</h3></li>';
                                      html += '</ul></div>';
                                  }

                                  $("#paginationRetiros").html(htmlPagination);
                                  $("#listaRetiros").html(html);
                                  //-- Selecciono la primer pagina del pagination, porque si no 
                                  $("#linkRetiros0").click();


        //------------------------------------------------------------------ Pagination de los reportes
                                  var htmlPagination = '';
                                  var html= '<div id="reportes0" class="tab-pane fade col-md-12 active in"><ul class="list-group"><ul>';
                              //------ Me fijo si voy a necesitar la navegacion lateral
                                  if(datos.usuario[0]['reportes'].length > 45){
                                      htmlPagination += '<li id="previous"><a href="#" onclick="cambiarPagination(\'previous\',\'paginaReportes\',\'linkReportes\',\'resultadoReportes\')" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                                      htmlPagination += '<li id="paginaReportes0" class="paginaReportes"><a id="linkReportes0" data-toggle="tab" aria-expanded="true" href="#reportes0">1</a></li>';
                                  }else{
                                      htmlPagination += '<li><a id="linkReportes0" data-toggle="tab" aria-expanded="true" href="#reportes0">1</a></li>';
                                  }
                                  // --- Creo una lista cada 5 reportes
                                  for(var $i=0; $i<datos.usuario[0]['reportes'].length; $i++ ){
                                              //--- Si pasan 5 reportes creo una lista
                                              if(($i%5 == 0)&&($i!=0)){
                                                //--- Agrego una pagina nueva en el pagination
                                                if((datos.usuario[0]['reportes'].length > 45)&&($i == 40)&&($i != datos.usuario[0]['reportes'].length -1)){                                      
                                                     htmlPagination += '<li><a href="#">...</a></li>';
                                                }
                                                //console.log("I: " + Math.ceil($i/5) + "   total: " + Math.ceil(datos.usuario[0]['reportes'].length/5-1));
                                                if(($i>=40)&&(Math.ceil($i/5)!=Math.ceil(datos.usuario[0]['reportes'].length/5-1))){
                                                 // console.log($i);
                                                    htmlPagination += '<li id="paginaReportes'+Math.trunc($i/5)+'" class="hidden paginaReportes"><a id="linkReportes'+Math.trunc($i/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination('+Math.trunc($i/5)+',\'paginaReportes\',\'linkReportes\',\'resultadoReportes\')">'+Math.trunc($i/5 +1) +'</a></li>';
                                                }else{                                        
                                                    htmlPagination += '<li id="paginaReportes'+Math.trunc($i/5)+'" class="paginaReportes"><a id="linkReportes'+Math.trunc($i/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination('+Math.trunc($i/5)+',\'paginaReportes\',\'linkReportes\',\'resultadoReportes\')">'+Math.trunc($i/5 +1) +'</a></li>';              
                                                }                                                   
                                                //-- Agrego la lista al div contenedor
                                                $("#listaReportes").html(html);                                                
                                              }
                                              
                                              if(Math.trunc($i/5)==0)
                                                html+= '<li name="resultadoReportes" data-toggle="collapse" style="display: block;" data-parent="#reportes'+Math.trunc($i/5)+'" class="resultadoReportes'+Math.trunc($i/5)+' list-group-item row"><div data-toggle="collapse" data-parent="#reportes'+Math.trunc($i/5)+'" class="col-md-8" ><p><b>Fecha:</b>  ' +datos.usuario[0]['reportes'][$i].fechahora + '</p></div><div data-toggle="collapse" data-parent="#listaReportes" class="col-md-8" ><p><b>Reporte:</b> '+datos.usuario[0]['reportes'][$i].tipoaccion+'</p></div></li>'; 
                                              else
                                                html+= '<li name="resultadoReportes" data-toggle="collapse" style="display: none;" data-parent="#reportes'+Math.trunc($i/5)+'" class="resultadoReportes'+Math.trunc($i/5)+' list-group-item row"><div data-toggle="collapse" data-parent="#reportes'+Math.trunc($i/5)+'" class="col-md-8" ><p><b>Fecha:</b>  ' +datos.usuario[0]['reportes'][$i].fechahora + '</p></div><div data-toggle="collapse" data-parent="#listaReportes" class="col-md-8" ><p><b>Reporte:</b> '+datos.usuario[0]['reportes'][$i].tipoaccion+'</p></div></li>'; 
                                    
                                  }
                                  if(datos.usuario[0]['reportes'].length > 45){
                                    htmlPagination += '<li><a href="#" onclick="cambiarPagination(\'next\',\'paginaReportes\',\'linkReportes\',\'resultadoReportes\')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                                  }

                                  if(datos.usuario[0]['reportes'].length == 0){
                                      html = '<ul><li data-toggle="collapse" data-parent="#listaRetiros" name="" class="list-group-item row"><h3>No se encontraron resultados</h3></li>';
                                      html += '</ul></div>';
                                  }

                                  $("#paginationResportes").html(htmlPagination);
                                  $("#listaReportes").html(html);
                                  //-- Selecciono la primer pagina del pagination, porque si no 
                                  $("#linkReportes0").click();

      //-------------------------------------------------------------------------------------------------------------
                          });
                    }
                });
          $('#modalVerUsuarioAdministrador').modal('show');
  }else{


                $.ajax({
                  type: "POST",cache: false,   dataType:'json', data: {"t":"traerUsuario","idUsuario":id},
                  url: "./api.php", success: function(datos,texto,jqXHR){
                          $(document).ready(function(){



                                  $("#tituloModalUsuariosVen").text("Información del usuario");
                                  //-------------------------
                                  $("#idUsuarioV").val(datos.usuario[0].id);
                                  $("#nombreUsuarioV").html(datos.usuario[0].nombre);
                                  $("#apellidoUsuarioV").html(datos.usuario[0].apellido);
                                  $("#dniUsuarioV").html(datos.usuario[0].dni);            
                                  $("#observacionesUsuarioV").html(datos.usuario[0].observacion);

                                  if(datos.usuario[0].bloqueado == 0)
                                      $("#estadoUsuarioV").html("Activo");
                                  else
                                      $("#estadoUsuarioV").html("Bloqueado");
                                  
                                  $("#tipoUsuarioV").html("Vendedor");
                                 

                                /////////////////Traigo las cajas del usuario
                                var html='<p class="text-muted" style="padding:2%"> Se muestran las cajas ordenadas por fecha, podrá acceder a los detalles de la misma haciendo click en "Ver Caja".</p>';
                                $('#listaCajas').html(html);
                                var htmlPagination = '';
                                html= '<div id="cajas0" class="tab-pane fade col-md-12 active in"><ul class="list-group"><ul>';
                                
                                if(datos.cajas.length > 45){
                                    htmlPagination += '<li id="previous"><a href="#" onclick="cambiarPagination(\'previous\',\'paginaCajas\',\'linkCajass\')" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                                    htmlPagination += '<li id="paginaCajas0" class="paginaCajas"><a id="linkCajas0" data-toggle="tab" aria-expanded="true" href="#cajas0">1</a></li>';
                                }else{
                                    htmlPagination += '<li><a id="linkCajas0" data-toggle="tab" aria-expanded="true" href="#cajas0">1</a></li>';
                                }
                                  
                                        
                                for(var i=0; i<datos.cajas.length; i++)
                                {
                                      if((i%5 == 0)&&(i!=0)){
                                            //--- Agrego una pagina nueva en el pagination
                                            if((datos.cajas.length > 45)&&(i == 40)){                                      
                                                 htmlPagination += '<li><a href="#">...</a></li>';
                                            }
                                            //console.log("I: " + Math.ceil($i/5) + "   total: " + Math.ceil(datos.usuario[0]['reportes'].length/5-1));
                                            if((i>=40)&&(Math.ceil(i/5)!=Math.ceil(datos.cajas.length/5-1))){
                                             // console.log($i);
                                                htmlPagination += '<li id="paginaCajas'+Math.trunc(i/5)+'" class="hidden paginaCajas"><a id="linkCajas'+Math.trunc(i/5)+'" data-toggle="tab" aria-expanded="true" href="#cajas' + Math.trunc(i/5) + '">'+Math.trunc(i/5 +1) +'</a></li>';
                                            }else{                                        
                                                htmlPagination += '<li id="paginaCajas'+Math.trunc(i/5)+'" class="paginaCajas"><a id="linkCajas'+Math.trunc(i/5)+'" data-toggle="tab" aria-expanded="true" href="#cajas' + Math.trunc(i/5) + '">'+Math.trunc(i/5 +1) +'</a></li>';              
                                            }      
                                            //--- cierro la tabla y creo una nueva
                                            html+='</ul></div>';
                                            //-- Agrego la tabla al div contenedor
                                            $("#listaCajas").html(html);

                                            html += '<div id="cajas'+Math.trunc(i/5)+'" class="tab-pane fade col-md-12 active in"><ul class="list-group"><ul>';                                        
                                      }

                                      html+= '<li data-toggle="collapse" data-parent="#cajas'+Math.trunc(i/5)+'" name="" class="list-group-item row"><div data-toggle="collapse" data-parent="#cajas'+Math.trunc(i/5)+'" class="col-md-8" ><p><b>Fecha apertura:</b>  ' +formatoFecha(datos.cajas[i].fechahoraapertura) + '</p></div><div data-toggle="collapse" data-parent="#listaReportes" class="col-md-8" ><p><b>Fecha cierre:</b> '+formatoFecha(datos.cajas[i].fechahoracierre)+'</p></div><div class="col-md-10"><div class="col-md-5"><p><b>Monto inicial:</b>'+datos.cajas[i].montoinicial+'</p></div><div class="col-md-5"><p><b>Monto final:</b>'+datos.cajas[i].montofinal+'</p></div></div><a onclick="javascript:mostrarCaja('+datos.cajas[i].id+');">Ver Caja</a></li>'; 
                                      
                                      
                                     
                                    //d  html=html+'<a href="javascript:mostrarCaja('+datos.cajas[i].id+');">Ver Caja</a></td>';
                                }
                                //html+='</tbody></table>';
                                //$('#verCajas').html(html);
                                if(datos.cajas.length > 45){
                                    htmlPagination += '<li><a href="#" onclick="cambiarPagination(\'next\',\'paginaCajas\',\'linkCajas\')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                                }

                                if(datos.cajas.length == 0){
                                    html = '<ul><li data-toggle="collapse" data-parent="#verVentas" name="" class="list-group-item row"><h3>No se encontraron resultados</h3></li>';
                                    html += '</ul></div>';
                                }

                                  $("#paginationCajas").html(htmlPagination);
                                  $("#listaCajas").html(html);
                                  //-- Selecciono la primer pagina del pagination, porque si no 
                                  $("#linkCajas0").click();
                        });
                    }
                });
          $('#modalVerUsuarioVendedor').modal('show');
  }
}



//---------------------------------------- ABM Usuarios  -------------------------------
//-- Esta funcion se encarga de guardar las observaciones de los vendedores
function guardarObservacion(){
    $.ajax({
                type: "POST",cache: false,   dataType:'json', data: {"t":"guardarObservacion","idUsuario":$('#idUsuarioV').val(), "observacion" : $('#observacionesUsuarioV').val()},
                url: "./api.php", success: function(datos,texto,jqXHR){
                  if(datos.err == 0)
                  {
                        swal("Buen trabajo!", "Se ha agregado una observación al usuario correctamente!", "success");
                   

                        //$("#botonNuevoUsuario").click();
                        //$().initPanelUsuarios();
                        //$("#errorUsuario").empty();
                        //$("#error").hide();

                  }
              }
    });
}




function guardarUsuario(){
    var error=0;
    $("#errorUsuario").hide();
    $("#errorUsuario").empty();
    $("#errorUsuario").append('<b>Por favor mire los siguiente errores</b></br>');


        if(document.getElementById('nombreUsuario').value == ''){
          error=1;
           $("#errorUsuario").append('Por favor completar el nombre</br>');
        }
        if(document.getElementById('dniUsuario').value == ''){
          error=1;
           $("#errorUsuario").append('Por favor colocar el dni.</br>');
        }
        if(document.getElementById('contrasena').value == ''){
          error=1;
           $("#errorUsuario").append('Por favor colocar la contraseña.</br>');
        }
        if(document.getElementById('apellidoUsuario').value == ''){
          error=1;
           $("#errorUsuario").append('Por favor colocar el Apellido.</br>');
        }
    
        if(document.getElementById('contrasena').value != document.getElementById('contrasena2').value){
            error = 1;
            $("#errorUsuario").append('Las contraseñas no coinciden.</br>');
        }

        if(error==0)
        $.ajax({
                type: "POST",cache: false,   dataType:'json', data: {"t":"guardarUsuario","idUsuario":$('#idUsuario').val(), "nombreUsuario":$('#nombreUsuario').val(), "apellidoUsuario":$('#apellidoUsuario').val(),"passUsuario":$('#contrasena').val(), "dniUsuario":$('#dniUsuario').val(),"tipoUsuario":$('#selectTipoUsuario').val()},
                url: "./api.php", success: function(datos,texto,jqXHR){
                  if(datos.err == 0)
                  {
                        if($("#idPaciente").val() != '')
                          swal("Buen trabajo!", "El usuario ha sido modificado correctamente!", "success");
                        else
                          swal("Buen trabajo!", "El usuario ha sido agregado correctamente!", "success");

                        $("#botonNuevoUsuario").click();
                        vaciarFormularios("usuarios");
                        $().initPanelUsuarios();
                        $("#errorUsuario").empty();
                        $("#error").hide();

                  }
                  else if(datos.err == 1)
                  {
                    $("#errorUsuario").empty();
                    $("#errorUsuario").append('<b>Por favor mire los siguiente errores</b></br>');
                    $("#errorUsuario").append(datos.txerr);
                    $("#errorUsuario").show();


                  }
                }
        });
      else
        $("#errorUsuario").show();

}

function bloquearUsuario(id){
    $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"bloquearUsuario","idUsuario":id},
    url: "./api.php", success: function(datos,texto,jqXHR){

    }
    });
    $().initPanelUsuarios();
}

function desbloquearUsuario(id){
    $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"desbloquearUsuario","idUsuario":id},
    url: "./api.php", success: function(datos,texto,jqXHR){

    } 
    });
    $().initPanelUsuarios();
}

function modificarUsuario(id){
        $.ajax({
          type: "POST",cache: false,   dataType:'json', data: {"t":"traerUsuario","idUsuario":id},
          url: "./api.php", success: function(datos,texto,jqXHR){
                  $(document).ready(function(){
                          //$("#botonNuevoUsario").click();
                          console.log("Entro en modificar usurio");
                           vaciarFormularios("usuarios");
                          $("#tituloModal").text("Modificar Usuario");

                          //-------------------------
                          $("#idUsuario").val(datos.usuario[0].id);
                          $("#nombreUsuario").val(datos.usuario[0].nombre);
                          $("#apellidoUsuario").val(datos.usuario[0].apellido);
                          $("#dniUsuario").val(datos.usuario[0].dni);
                          $("#contrasena").val(datos.usuario[0].pass);
                          $("#contrasena2").val(datos.usuario[0].pass);
                          document.getElementById("selectTipoUsuario")[datos.usuario[0].tipo].selected = true;
                          
                  });
            }
        });
       
}


