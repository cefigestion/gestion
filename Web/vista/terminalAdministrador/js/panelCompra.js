function mostrarCompra(id){
	$.ajax({  //traigo los datos de la caja
                    type: "POST",cache: false,   dataType:'json', data: {"t":"mostrarCompra", "idCompra":id},
                    url: "./api.php", 
                    success: function(datos,texto,jqXHR){
                    	////////////////////////////Datos de la compra
                        	var html='<div class="row" style="background-color:white"><div class="col-md-6"><br><b>Fecha de compra: </b>'+formatoFecha(datos.compra[0].fecha)+'<br><br>';
                    	//html=html+'<b>Fecha de Cierre: </b>'+formatoFecha(datos.caja[0].fechahoracierre)+'<br><br>';
                    	html=html+'<b>Total: </b>'+datos.compra[0].cantidad+'<br><br></div>';

                    	/////////////////////////Datos del proveedor
                    	html+='<div class="col-md-6"><h4><b>Proveedor:</b></h4><b>Razon social: </b>';
                    	html=html+datos.proveedor[0].razonsocial+'<br>';
                    	html=html+'<b>CUIT : </b>'+datos.proveedor[0].cuit+'<br>';
                    	html+='<b>Telefono: </b>';
                    	html+=datos.proveedor[0].telefono+'<br>';
                         html+='<b>E-mail: </b>';
                         html+=datos.proveedor[0].email;
                    	html+='</div></div>';
                    	$("#contenedorDatosCompra").html(html);

                    	/////////////////////////Productos de Compra
                    	html='<table class="table table-hover" style="background-color:white"><thead><tr><th>Producto</th><th>Cantidad</th><th>Precio de Compra</th><th>Total</th></tr></thead><tbody>';

                         console.log(datos.productos.length);
                    	for(var i=0; i<datos.productos.length;i++)
                    	{
                    		html+='<tr><td>';
                    		html+=datos.productos[i].nombre;
                    		html+='</td><td>';
                    		html+=datos.productos[i].cantidad;
                    		html+='</td><td>';
                    		html+=datos.productos[i].precio;
                              html+='</td><td>';
                              html+=datos.productos[i].cantidad*datos.productos[i].precio +'</td></tr>';
                    	}
                    	html+='</tbody></table>';
                    	$("#contenedorCompraProductos").html(html);


                    	$("#modalMostrarCompra").modal("show");
                    }
            });
}