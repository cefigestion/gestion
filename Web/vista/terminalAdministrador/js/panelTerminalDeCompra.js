$.fn.initTerminalDeCompra = function () {

  $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"traerTodosProveedores"},
    url: "./api.php", success: function(datos,texto,jqXHR){
      $("#panelBusqueda").empty();
      $("#paginationContenedor").empty();

     /* $("#panelBusqueda").append(
          '<div class="col-lg-8" style="margin-left: 0%"><div class="input-group"><input type="text" class="form-control" id="busBuscarProducto" placeholder="Producto...." onkeyup="cargarResultadoXclase(\'busBuscarProducto\',\'resultadoProductos\')"><span class="input-group-btn"><button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalNuevoProducto" style="background-color: #2980B9; color: #ECF0F1">Buscar</button></span></div></div><div class="col-lg-4"><button class="btn btn-block btn-primary" name="botonNuevo" id="botonNuevoProducto" onclick="vaciarFormularios(\'productos\')" data-toggle="modal"  data-target="#modalNuevoProducto" style="background-color: #3498DB; border-color: #3498DB; color: #ECF0F1;"><span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 5%"></span><span class="glyphicon glyphicon-open-file" aria-hidden="true"></span></button></div>'
      );*/

      $("#panelContenedor").empty();

      var html= '<div class="panel-heading" style="text-align: center;font-size: 16px;"><b>Seleccione el Proveedor al cual se realizo la compra.<b></b></b></div>';
      $("#panelContenedor").append(html);
      html="";

      for (var i = 0; i < datos.proveedores.length; i++) {
        if(datos.proveedores[i].borrado!= 1){
            html+="<option value='"+datos.proveedores[i].id+"'>"+datos.proveedores[i].razonsocial+"</option>"
        }
    }

    $("#panelContenedor").append('<div class="input-group" style="width: 60%;margin-left:20%;margin-top:5%;margin-bottom: 5%;height: 50px;"><span class="input-group-addon" style="width: 30%;background-color: #3498DB; color:#ECF0F1; border-color: #3498DB" id="basic-addon1">Proveedor:</span><select id="seleccionarProveedor" name="seleccionarProveedor" class="form-control"style="height: 50px;">'+html+'</select></div>');
    $("#panelContenedor").append('<div style="margin-left:20%;"><button id="botonSeleccionarProveedor" class="btn btn-block btn-primary" style="width:70%;background-color: #8BC34A; border-color: #7CB342; color: #ECF0F1;margin:3%;font-size:16px;"><span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 10px;"></span><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span></button></div>');
      
/* SCRIPT QUE MANEJA EL EVENTO DEL CLICK EN EL VENTO DE SELECCIONAR PROVEEDOR */
    $("#panelContenedor").append('<script>$("#botonSeleccionarProveedor").click(function(){if($("#seleccionarProveedor").val()!=\'\'){$("#panelContenedor").innerHTML= \'\';$("#panelBusqueda").innerHTML= \'\';$().initMostrarTerminalCompra($("#seleccionarProveedor").val());}});</script>');
      }
  }); 
}; 

function initMostrarTerminalCompra(idProveedor){
$.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"initTerminalCompra","idProveedor": idProveedor},
    url: "./api.php", success: function(datos,texto,jqXHR){
      var html='';
       //CargamosProductos
       for(var i=0;i<datos.productos.length;i++)
       {
        html+='<tr name="resultadoProd" title=\'tablaproducto'+datos.productos[i].id+'\'>';
        html+='<td style="text-align: center;vertical-align: middle;">'+datos.productos[i].id+'</td>'; 
        html+='<td style="text-align: center;vertical-align: middle;">'+datos.productos[i].nombre+'</td>'; 
        html+='<td style="text-align: center;vertical-align: middle;width:20%">'; 
        if(datos.productos[i].tipoventa==1) 
          html+='$ ' +datos.productos[i].cantidadactual; 
        else
          html+=datos.productos[i].cantidadactual + ' unidades'; 
        html+='<td style="text-align: center;vertical-align: middle;">'+datos.productos[i].preciocompra+'</td>'; 
        html+='<td style="text-align: center;vertical-align: middle;"><div class="col-lg-12">'; 
        var boton='<button onclick="agregarProducto('+datos.productos[i].id+');" class="btn btn-block btn-primary"  style="background-color: #8BC34A; border-color: #7CB342; color: #ECF0F1"><span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 5%"></span></span></button>';
        html+=boton;
        html+='</td>'; 
        html+='</tr>';      
       }

       $('#containerproductos').html(html);  
       $('#containerProductosVenta').html('');  
$('#idProveedor').val(idProveedor);

      }
  });

$.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"traerUsuarioActual"},
    url: "./api.php", success: function(datos,texto,jqXHR){
      var html=datos.usuario[0].apellido+', '+datos.usuario[0].nombre;

       $('#nombreapellidosusuario').html(html);  

      }
  });


actualizarTotal();
};

$.fn.initMostrarTerminalCompra = function (idProveedor) {

  $("#mainPanel").load("vista/terminalAdministrador/terminalCompra.html");
  $("#menuMain").load("vista/terminalAdministrador/menuTerminalCompra.html");
  $("#mainTitulo").html("Terminal de Compras - CEFI Administración");


  initMostrarTerminalCompra(idProveedor);
}; 


//-------------------------------- Control de los campos de modal de la carga de productos ---------------------------
function agregarProducto(id = null){
      var contenedor = document.getElementById('contenedorProductos');
      var productos = document.getElementsByClassName('Producto');
      if(productos.length > 1){
        var ultimoId = productos[productos.length-1].title *1;
        var ultimoId = ultimoId + 1;
      }else
        var ultimoId = 2;
      var ultimoAumento = 34 * productos.length;
      var html = '<tr class="Producto" title=\''+ultimoId+'\'>';
      html += '<td align="center"><input id="nombreProducto'+ultimoId +'" type="text" class="form-control" placeholder="Nombre" aria-describedby="basic-addon1"></td>';
      html += '<td align="center"><select class="form-control" id="tipoVenta'+ultimoId+'" onchange="ajustarTipoVenta(\''+ultimoId+'\')" style="width:145%" name="tipoVenta'+ultimoId+'[]"><option value="0">Unidades</option><option value="1">Peso ($)</option></select> </td>';
      html += '<td><input id="cantidadProducto'+ultimoId +'" onchange="calcularPrecioVenta(\'cant\',\''+ultimoId+'\')" type="number" style="width: 85%; float: right;" step="1" class="form-control" placeholder="Stock Actual" aria-describedby="basic-addon1"  value="0"></td>';
      html += '<td><input id="precioCompra'+ultimoId +'" onchange="calcularPrecioVenta(\'pc\',\''+ultimoId+'\')" type="number" step="1" class="form-control" placeholder="Precio de la compra del producto" aria-describedby="basic-addon1"  value="0"></td>';
      html += '<td><input id="porcentajeVenta'+ultimoId +'" onchange="calcularPrecioVenta(\'com\',\''+ultimoId+'\')" type="number" step="1" class="form-control" placeholder="Comisión" aria-describedby="basic-addon1"  value="0"></td>';
      html += '<td><input id="precioProducto'+ultimoId +'" onchange="calcularPrecioVenta(\'pv\',\''+ultimoId+'\')" type="number" step="0,01" class="form-control" placeholder="Precio de venta" aria-describedby="basic-addon1"></td>';
      html += '<td><button type="button" class="btn btn-default" style="position:fixed; !important; !important;" onclick="eliminarCampoProducto('+ultimoId+');"><span class="glyphicon glyphicon-remove" style="color: #E74C3C" aria-hidden="true"></span></button></td>'; 
      html += '</tr>';

      
      $('#contenedorProductos').append(html); 

}

function eliminarCampoProducto(id){
      var productos = document.getElementsByClassName('Producto');
      for(var i=0;i < productos.length; i++)
        if(id == productos[i].title)
          productos[i].outerHTML  = '';

}

//----------- Calcula el precio de la venta o el porcentaje de comision, se le pasa el elemente que lo genero pc= precio compra, pv= precio venta, com= porcentaje de la comision.
function calcularPrecioVenta(generador,element){
  var preciocompra = document.getElementById('precioCompra' + element).value;
  var comision = document.getElementById('porcentajeVenta' + element).value; 
  var precioventa = document.getElementById('precioProducto'+ element).value;
  var cantidad = document.getElementById('cantidadProducto'+ element).value;
  var tipoventa = document.getElementById('tipoVenta'+ element).value;

  if(tipoventa==1)
  {    
    switch(generador){
        case 'com':
                    if(cantidad != 0){
                        document.getElementById('cantidadProducto'+ element).value = (cantidad * (comision/100) + cantidad*1).toFixed(2);
                    }
              break;
        case 'cant':
                    if(comision != 0){
                        document.getElementById('porcentajeVenta'+ element).value = (cantidad * (comision/100) + cantidad*1).toFixed(2);
                    }
              break;
    }
  }
  else
  {
  //----------------- toFixed() funcion para truncar decimales
    switch(generador){
        case 'pc':
                    if(comision!= 0){
                        document.getElementById('precioProducto'+ element).value = (preciocompra * (comision/100) + preciocompra*1).toFixed(2);
                    }
              break;
        case 'com':
                    if(preciocompra != 0){
                        document.getElementById('precioProducto'+ element).value = (preciocompra * (comision/100) + preciocompra*1).toFixed(2);
                    }
              break;

        case 'pv':
                    if(preciocompra != 0){
                      document.getElementById('porcentajeVenta'+ element).value = ((precioventa*100)/(preciocompra*1) - 100).toFixed(2);
                    }

              break;
    }

  }
}

//----------- Ajustar tipo de venta
function ajustarTipoVenta(element){
  var preciocompra = document.getElementById('precioCompra' + element);
  var comision = document.getElementById('porcentajeVenta' + element).value; 
  var precioventa = document.getElementById('precioProducto'+ element);
  var cantidad = document.getElementById('cantidadProducto'+ element).value;
  var tipoventa = document.getElementById('tipoVenta'+ element).value;

  if(tipoventa==1)
  {    
    preciocompra.value=1;
    preciocompra.type='hidden';
    precioventa.value=1;  
    precioventa.type='hidden';
  }
  else
  {
    preciocompra.value=1;
    preciocompra.type='number';
    precioventa.value=1;
    precioventa.type='number';
  }
}


//---------------------------------------- ABM productos  -------------------------------

function guardarProductos(){
    var error=0;
    $("#error").hide();
    $("#error").empty();
    $("#error").append('<b>Por favor mire los siguiente errores</b></br>');



    var productos = document.getElementsByClassName('Producto');
    var arrayP = new Array();
    var arrayNombre = new Array();
    var arrayTipoVenta = new Array();
    var arrayPrecioCompra = new Array();
    var arrayporcentajeVenta = new Array();
    var arrayCantidad = new Array();
    var arrayPrecio = new Array();

    console.log(productos.length);

  
    for(var i=1; i<=productos.length;i++){

        arrayNombre[i-1] = document.getElementById('nombreProducto'+i).value;
        arrayTipoVenta[i-1] = document.getElementById('tipoVenta'+i).value;
        arrayPrecioCompra[i-1] = document.getElementById('precioCompra'+i).value;
        arrayporcentajeVenta[i-1] = document.getElementById('porcentajeVenta'+i).value;
        arrayCantidad[i-1] = document.getElementById('cantidadProducto'+i).value;
        arrayPrecio[i-1] = document.getElementById('precioProducto'+i).value;

        if(document.getElementById('nombreProducto'+i).value == ''){
          error=1;
           $("#error").append('Por favor completar el campo "Nombre" del producto de la linea '+ i +'</br>');
        }
        if(document.getElementById('precioProducto'+i).value == ''){
          error=1;
           $("#error").append('Por favor completar el campo "Precio" del producto de la linea '+ i +'</br>');
        }
    }



        if(error==0)
        $.ajax({
                type: "POST",cache: false,   dataType:'json', data: {"t":"guardarProductos","idProducto":$('#idProducto').val(), "arrayNombre":JSON.stringify(arrayNombre), "arrayTipoVenta":JSON.stringify(arrayTipoVenta),"arrayporcentajeVenta":JSON.stringify(arrayporcentajeVenta),"arrayPrecioCompra":JSON.stringify(arrayPrecioCompra), "arrayCantidad":JSON.stringify(arrayCantidad),"arrayPrecio":JSON.stringify(arrayPrecio)},
                url: "./api.php", success: function(datos,texto,jqXHR){
                  if(datos.err == 0)
                  {
                        if($("#idPaciente").val() != '')
                          swal("Buen trabajo!", "El producto ha sido modificado correctamente!", "success");
                        else
                          swal("Buen trabajo!", "El producto ha sido agregado correctamente!", "success");

                        $("#botonNuevoProducto").click();
                        vaciarFormularios("productos");
                        $().initPanelProductos();
                        $("#error").empty();
                        $("#error").hide();

                  }
                  else if(datos.err == 1)
                  {
                    $("#error").empty();
                    $("#error").append('<b>Por favor mire los siguiente errores</b></br>');
                    $("#error").append(datos.txerr);
                    $("#error").show();


                  }
                }
        });
      else
        $("#error").show();

}

function modificarProducto(id){
        $.ajax({
          type: "POST",cache: false,   dataType:'json', data: {"t":"traerProducto","idProducto":id},
          url: "./api.php", success: function(datos,texto,jqXHR){
                  $(document).ready(function(){//$("#botonNuevoProducto").click();
                          console.log("Entro en modificar producto"+datos.producto[0].tipoventa);
                          console.log("Entro en modificar producto");
                          vaciarFormularios("productos");
                          $("#tituloModal").text("Modificar Producto");
                          //--- Quito el boton para agregar mas productos -----
                          $("#botonAgregarProducto").empty();
                          //-------------------------
                          //--- Me fijo si debo ocultar algun campo
                          if(datos.producto[0].tipoventa==0){
                                document.getElementById('precioCompra1').type = "number";
                                $("#precioCompra1").val(datos.producto[0].preciocompra);
                                document.getElementById('porcentajeVenta1').type = "number";
                                $("#porcentajeVenta1").val(datos.producto[0].comision);
                          }else{
                              document.getElementById('precioCompra1').type = "hidden";
                              document.getElementById('porcentajeVenta1').type = "number";                          
                          }

                          $("#idProducto").val(datos.producto[0].id);
                          $("#nombreProducto1").val(datos.producto[0].nombre);
                          document.getElementById("tipoVenta1")[datos.producto[0].tipoventa].selected = true;

                          $("#cantidadProducto1").val(datos.producto[0].cantidadactual);
                          $("#precioProducto1").val(datos.producto[0].precioventa);

                          ajustarTipoVenta(1);
                          //$().initPanelProductos();

                  });
            }
        });
        
}


function eliminarProducto(id){
        $.ajax({
          type: "POST",cache: false,   dataType:'json', data: {"t":"eliminarProducto","idProducto":id},
          url: "./api.php", success: function(datos,texto,jqXHR){

            }
        });
        $().initPanelProductos();
}
