function initTerminalAdministrador(){
  
  $.ajax({
      type: "POST",cache: false,   dataType:'json', data: {"t":"traerUsuarioActual"},
      url: "./api.php", success: function(datos,texto,jqXHR){
          $('#nombreUsuarioActual').append("Terminal de administración - Usuario actual: <b>" + datos.usuario[0].apellido + ", " + datos.usuario[0].nombre + "</b>");
      }
  });

};

function mostrarCerrarSesion(){
    swal({
  title: "Desea cerrar sesión?",
  showCancelButton: true,
  closeOnConfirm: false,
  inputPlaceholder: "Monto Final de la Caja..."
}, function (inputValue) {
  if (inputValue === false) return false;
  $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"cerrarSesion"},
    url: "./api.php", success: function(datos,texto,jqXHR){

        //swal("Genial!", "La sesión a sido cerrada!", "success");
        location.href = 'index.html';

      }
  });
  
});
}





function hacerReportes(){
  //Oculto la paginacion
  $("#paginationContenedor").empty();
  
  $("#panelContenedor").load("vista/terminalAdministrador/vistaReportes.html");
  $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"checkUser"},
    url: "./api.php", 
    success: function(datos,texto,jqXHR){
          if(datos.state==true && datos.permiso=='Administrador')
          {
                $.ajax({  //traigo los registros de errores
                    type: "POST",cache: false,   dataType:'json', data: {"t":"traerTodosRegistros"},
                    url: "./api.php", 
                    success: function(datos,texto,jqXHR){
                            var html='<p class="text-muted" style="padding:2%"> Se muestran los reportes de errores a la fecha del dia junto con la/s caja/s involucrada/s.</p>';
                            html+='<table class="table table-hover"><thead><tr><th>Fecha</th><th>Mensaje</th><th>Caja/s</th></tr></thead><tbody>';
                            for(var i=0; i<datos.registros.length; i++)
                            {
                                html+='<tr name="filaReporte"><td>';
                                html+=formatoFecha(datos.registros[i].fecha); //VER QUE HAYA UNA FECHA EN LA BD
                                html+='</td><td>';
                                html+=datos.registros[i].mensaje;
                                html+='</td><td>';
                                html=html+'<a onclick="javascript:mostrarCaja('+datos.registros[i].idcaja1+');">Caja '+datos.registros[i].idcaja1+'</a>';
                                if(datos.registros[i].idcaja2!=null)
                                  html=html+', <a onclick="javascript:mostrarCaja('+datos.registros[i].idcaja2+');">Caja '+datos.registros[i].idcaja2+'</a>';
                                html+='</td></tr>';
                            }
                            html+='</tbody></table>';
                            if(datos.registros.length>0)
                            {
                              $('#contenedorRegistros').html(html);
                            }
                    }
                  }); //finaliza ajax para traer los registros de errores


                  $.ajax({  //traigo las cajas
                    type: "POST",cache: false,   dataType:'json', data: {"t":"traerTodosCajas"},
                    url: "./api.php", 
                    success: function(datos,texto,jqXHR){
                            var html='<p class="text-muted" style="padding:2%"> Se muestran las cajas ordenadas por fecha, podrá acceder a los detalles de la misma haciendo click en "Ver Caja".</p>';
                            html+='<table class="table table-hover"><thead><tr><th>Fecha de Apertura</th><th>Fecha de Cierre</th><th>Monto Inicial</th><th>Monto Final</th><th></th></tr></thead><tbody>';
                            for(var i=0; i<datos.cajas.length; i++)
                            {
                              html+='<tr name="filaReporte"><td>';
                              html+=formatoFecha(datos.cajas[i].fechahoraapertura);
                              html+='</td><td>';
                              html+=formatoFecha(datos.cajas[i].fechahoracierre);
                              html+='</td><td>';
                              html+=datos.cajas[i].montoinicial;
                              html+='</td><td>';
                              html+=datos.cajas[i].montofinal;
                              html+='</td><td>';
                              html=html+'<a onclick="javascript:mostrarCaja('+datos.cajas[i].id+');">Ver Caja</a></td></tr>';
                            }
                            html+='</tbody></table>';
                            if(datos.cajas.length>0)
                            {
                              $('#contenedorCajas').html(html);
                            }                              
                    }
                  });//finaliza el ajax para traer las cajas


                  $("#panelBusqueda").html('<div class="col-lg-8" style="margin-left: 0%"><div class="input-group"><input type="date" class="form-control" id="busBuscarReporte" placeholder="dd/mm/aaaa" onchange="cargarResultadoReportes()"><span class="input-group-btn"><button class="btn btn-default" type="button" style="background-color: #2980B9; color: #ECF0F1" >Buscar</button></span>');


          } //if que verifica si soy un admin



    }
  });
  
} //Fin function hacer reportes


function formatoFecha(fecha="aaaa-mm-dd"){
  //Cambia el formato a dd/mm/aaaa
  var diaHora=fecha.split(" "); //Separa en dia y hora
  var rta="";
  switch(diaHora.length)
  {
    case 1:
      f=diaHora[0].split("-"); //Obtengo el año, mes y dia
      rta=f[2]+"/"+f[1]+"/"+f[0];
      break;

    case 2:
      f=diaHora[0].split("-"); //Obtengo el año, mes y dia
      rta=f[2]+"/"+f[1]+"/"+f[0]+" "+diaHora[1]; //Le concateno la hora si es que tiene
  }
  return rta;
}







