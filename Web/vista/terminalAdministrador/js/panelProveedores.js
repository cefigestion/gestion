$.fn.initPanelProveedores = function () {

  $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"traerTodosProveedores"},
    url: "./api.php", success: function(datos,texto,jqXHR){
      $("#panelBusqueda").empty();
      $("#paginationContenedor").empty();

      $("#panelBusqueda").append(
          '<div class="col-lg-8" style="margin-left: 0%"><div class="input-group"><input type="text" class="form-control" id="busBuscarProveedor" placeholder="Proveedor...." onkeyup="cargarResultadoXclase(\'busBuscarProveedor\',\'resultadoProveedores\')"><span class="input-group-btn"><button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalNuevoProveedor" style="background-color: #2980B9; color: #ECF0F1">Buscar</button></span></div></div><div class="col-lg-4"><button class="btn btn-block btn-primary" name="botonNuevo" id="botonNuevoProveedor"  onclick="vaciarFormularios(\''+'proveedores'+'\')"  data-toggle="modal"  data-target="#modalNuevoProveedor" style="background-color: #3498DB; border-color: #3498DB; color: #ECF0F1;"><span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 5%"></span><span class="glyphicon glyphicon-open-file" aria-hidden="true"></span></button></div>'
      );

      $("#panelContenedor").empty();




      var htmlPagination = '';
      var html= '';
      var cantTotal=0;
      for(var i = 0; i < datos.proveedores.length; i++){
        if(datos.proveedores[i].borrado != 1){
            cantTotal++;
             //console.log("CantTotal: "+cantTotal);
        }
      }
  //------ Me fijo si voy a necesitar la navegacion lateral
      if(cantTotal > 45){
          htmlPagination += '<li id="previous"><a onclick="cambiarPagination(\'previous\',\'paginaProveedores\',\'linkProveedores\',\'resultadoProveedores\')" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
          htmlPagination += '<li id="paginaProveedores0" class="paginaProveedores active"><a id="linkProveedores0" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination(\'0\',\'paginaProveedores\',\'linkProveedores\',\'resultadoProveedores\')">1</a></li>';
      }else{
          htmlPagination += '<li><a id="linkProveedores0" data-toggle="tab" class="paginaProveedores active" aria-expanded="true" onclick="cambiarPagination(\'0\',\'paginaProveedores\',\'linkProveedores\',\'resultadoProveedores\')">1</a></li>';
      }

      //---- Esta variable lleva la cuenta de los elementos que se mostraran, es para que no me queden paginas vacias que vendrian ser de los productos eliminador
      var noBorrados = 0;
      var cantPag = 1;

      for(var i = 0; i < datos.proveedores.length; i++){
        if(datos.proveedores[i].borrado != 1){
                  //--- Si pasan 5 reportes creo una lista
                    if((noBorrados%5 == 0)&&(noBorrados!=0)){

                      console.log("No borrados " + noBorrados);
                            //--- Agrego una pagina nueva en el pagination
                            if((cantTotal > 45)&&(noBorrados == 40)&&(noBorrados != cantTotal -1)){                                      
                                 htmlPagination += '<li><a href="#">...</a></li>';
                            }
                            if((noBorrados>=40)&&(Math.ceil(noBorrados/5)!=Math.ceil(cantTotal/5-1))&&(cantTotal/5-1!=cantPag)){                            
                                cantPag++;
                                htmlPagination += '<li id="paginaProveedores'+Math.trunc(noBorrados/5)+'" class="paginaProveedores hidden"><a id="linkProveedores'+Math.trunc(noBorrados/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination('+Math.trunc(noBorrados/5)+',\'paginaProveedores\',\'linkProveedores\',\'resultadoProveedores\')">'+Math.trunc(noBorrados/5 +1) +'</a></li>';
                            }else{     
                            cantPag++;                                   
                                htmlPagination += '<li id="paginaProveedores'+Math.trunc(noBorrados/5)+'" class="paginaProveedores"><a id="linkProveedores'+Math.trunc(noBorrados/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination('+Math.trunc(noBorrados/5)+',\'paginaProveedores\',\'linkProveedores\',\'resultadoProveedores\')">'+Math.trunc(noBorrados/5 +1) +'</a></li>';              
                            }      
                            //-- Agrego la lista al div contenedor
                            $("#listaProveedores").html(html);                            
                    }
            
            var popUpEliminar='swal({title: "¿Estas seguro?",text: "Un proveedor eliminado no podra ser recuperado.",type: "warning",showCancelButton: true,confirmButtonClass: "btn-danger",confirmButtonText: "Si, quiero eliminarlo!",closeOnConfirm: false},function(){swal("Eliminado!", "El proveedor ha sido eliminado correctamente.", "success");eliminarProveedor('+datos.proveedores[i].id+')});';
            var botonEliminar='<button type="button" onclick=\''+popUpEliminar+'\' class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB; margin-right:5%">Eliminar</button>';
            var botonModificar = '<button type="button" onclick="modificarProveedor('+datos.proveedores[i].id+')" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #2980B9; margin-right:5%" data-toggle="modal" data-target="#modalNuevoProveedor">Modificar</button>';
            var botonAñadirProducto = '<button type="button" onclick="mostrarModalAñadirProducto('+datos.proveedores[i].id+')" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB; margin-right:5%">Añadir producto</button>';           
            if(Math.trunc(noBorrados/5)==0){
              html+='<li name="resultadoProveedores" class="resultadoProveedores'+Math.trunc(noBorrados/5)+' list-group-item row" style="display: block;"><div class="col-md-10" style="padding-left: 2%; padding-right:2%"><div class="col-md-5"><p id='+ datos.proveedores[i].razonsocial+ '><b>Razon social:</b>  ' +datos.proveedores[i].razonsocial + '</p></div><div class="col-md-5"><b>E-mail:</b>  ' +datos.proveedores[i].email + '</p></div></div><div class="col-md-10"><div class="col-md-5"><p><b>Cuit:</b> '+ datos.proveedores[i].cuit+' </p></div><div class="col-md-5"><p><b>Telefono:</b> '+ datos.proveedores[i].telefono+' </p></div></div></div>'+botonModificar+botonEliminar+botonAñadirProducto+'</li>' ;
             //html+='<li name="resultadoProveedores" class="list-group-item row"><div class="col-md-12"><div class="col-md-6"><b>Razon social:</b>  ' +datos.proveedores[i].razonsocial+'</div><div class="col-md-6"><b>Cuit:</b> '+ datos.proveedores[i].cuit +'</div></div>';
           }else{
              html+='<li name="resultadoProveedores" class="resultadoProveedores'+Math.trunc(noBorrados/5)+' list-group-item row" style="display: none;"><div class="col-md-10" style="padding-left: 2%; padding-right:2%"><div class="col-md-5"><p id='+ datos.proveedores[i].razonsocial+ '><b>Razon social:</b>  ' +datos.proveedores[i].razonsocial + '</p></div><div class="col-md-5"><b>E-mail:</b>  ' +datos.proveedores[i].email + '</p></div></div><div class="col-md-10"><div class="col-md-5"><p><b>Cuit:</b> '+ datos.proveedores[i].cuit+' </p></div><div class="col-md-5"><p><b>Telefono:</b> '+ datos.proveedores[i].telefono+' </p></div></div></div>'+botonModificar+botonEliminar+botonAñadirProducto+'</li>' ;
           }
            //html+='<div class="col-md-12"><div class="col-md-6"><br><b>Telefono:</b> '+ datos.proveedores[i].telefono+'</div><div class="col-md-6"><br><b>E-mail:</b> '+ datos.proveedores[i].email+'</div></div>';
           // html=html+'<div class="col-md-12 col-md-offset-3"><br>'+botonModificar+botonEliminar+botonAñadirProducto+'</div></li>';

           noBorrados++;

        }
      }

    if(cantTotal > 45){
      htmlPagination += '<li id="next"><a onclick="cambiarPagination(\'next\',\'paginaProveedores\',\'linkProveedores\',\'resultadoProveedores\')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
    }

    if(cantTotal == 0){
        html = '<ul><li data-toggle="collapse" data-parent="#listaProveedores" name="" class="list-group-item row"><h3>No se encontraron resultados</h3></li>';
        html += '</ul>';
    }

    $("#paginationContenedor").html(htmlPagination);
    $("#panelContenedor").html(html);
      }
  }); 
}; 


//---------------------------------------- ABM productos  -------------------------------

function guardarProveedor(){
   var error=0;
    $("#errorProveedor").hide();
    $("#errorProveedor").empty();
    $("#errorProveedor").append('<b>Por favor mire los siguiente errores</b></br>');


        if(document.getElementById('razonsocialProveedor').value.trim() == ''){
          error=1;
           $("#errorProveedor").append('Por favor completar la razón social</br>');
        }

        var isnum = /^\d+$/.test($('#cuitProveedor').val());
        if(($('#cuitProveedor').val() != '')&&((($('#cuitProveedor').val().length < 11))||($('#cuitProveedor').val().length > 12))){
           error=1;
           $("#errorProveedor").append('Por favor, revisar el cuit.</br>');
        }

       isnum = /^\d+$/.test($('#telefonoProveedor').val());
        if($('#telefonoProveedor').val()!=''){
          if(!isnum){
            error=1;
            $("#errorProveedor").append('El numero telefonico debe contener solo numeros.</br>');
          }
        }/*
        if(document.getElementById('apellidoUsuario').value == ''){
          error=1;
           $("#errorProveedor").append('Por favor colocar el Apellido.</br>');
        }
    
        if(document.getElementById('contrasena').value != document.getElementById('contrasena2').value){
            error = 1;
            $("#errorProveedor").append('Las contraseñas no coinciden.</br>');
        }
*/

        if(error==0)
        $.ajax({
                type: "POST",cache: false,   dataType:'json', data: {"t":"guardarProveedor","idProveedor":$('#idProveedor').val(), "razonSocialProveedor":$('#razonsocialProveedor').val(), "cuitProveedor":$('#cuitProveedor').val(),"telefonoProveedor":$('#telefonoProveedor').val(), "emailProveedor":$('#emailProveedor').val()},
                url: "./api.php", success: function(datos,texto,jqXHR){
                  if(datos.err == 0)
                  {
                        if($("#idProveedor").val() != '')
                          swal("Buen trabajo!", "El proveedor ha sido modificado correctamente!", "success");
                        else
                          swal("Buen trabajo!", "El proveedor ha sido agregado correctamente!", "success");

                        $("#botonNuevoProveedor").click();
                        vaciarFormularios("proveedores");
                        $().initPanelProveedores();
                        $("#errorProveedor").empty();
                        $("#error").hide();

                  }
                  else if(datos.err == 1)
                  {
                    $("#errorProveedor").empty();
                    $("#errorProveedor").append('<b>Por favor mire los siguiente errores</b></br>');
                    $("#errorProveedor").append(datos.txerr);
                    $("#errorProveedor").show();


                  }
                }
        });
      else
        $("#errorProveedor").show();
}


function modificarProveedor(id){
  console.log("id  "+id);
  $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"traerProveedor","idProveedor":id},
    url: "./api.php", success: function(datos,texto,jqXHR){
            $(document).ready(function(){
                    //$("#botonNuevoUsario").click();
                    console.log("Entro en modificar proveedor");
                     vaciarFormularios("proveedores");
                    $("#tituloModal").text("Modificar proveedor");

                    //-------------------------
                    $("#idProveedor").val(id);
                    $("#razonsocialProveedor").val(datos.proveedor[0].razonsocial);
                    $("#cuitProveedor").val(datos.proveedor[0].cuit);
                    $("#telefonoProveedor").val(datos.proveedor[0].telefono);
                    $("#emailProveedor").val(datos.proveedor[0].email);                                                    
                    
            });
      }
  });
       
}


function eliminarProveedor(id){
    $.ajax({
      type: "POST",cache: false,   dataType:'json', data: {"t":"eliminarProveedor","idProveedor":id},
      url: "./api.php", success: function(datos,texto,jqXHR){

        }
    });
    $().initPanelProveedores();
}

function mostrarModalAñadirProducto(idProveedor){
  $.ajax({
      type: "POST",cache: false,   dataType:'json', data: {"t":"traerTodosProductos","idProveedor":idProveedor},
      url: "./api.php", success: function(datos,texto,jqXHR){
            //Traigo todos los productos al modal para que los pueda añadir, si ya existiese la relacion producto x proveedor, se controla cuando se vaya a insertar en la bd
            var filas="";
            var exi = false;
            for(var i=0; i<datos.productos.length; i++)
            {
              if(datos.productos[i].borrado==0){
                  filas=filas+"<tr name='resultadoProd'><td align='center'>"+datos.productos[i].id+"</td><td align='center'>"+datos.productos[i].nombre+"</td><td align='center'>";
                  exi = false;
                  for(var j = 0; j< datos.productos[0]["idProductoProveedor"].length; j++){
                      if(datos.productos[0]["idProductoProveedor"][j].id == datos.productos[i].id)
                        exi=true;
                  }

                  if(exi==true){
                      console.log("ENTRO PRODUCTO PROV");
                      filas=filas+'<input type="checkbox" value='+datos.productos[i].id+' checked></td></tr>';              
                  }else{
                    console.log("ENTRO PRODUCTO PROV sin marcar ---- CANT  elementos: " + datos.productos[0]["idProductoProveedor"].length);
                     filas=filas+'<input type="checkbox" value='+datos.productos[i].id+'></td></tr>';
                  }
              }
            }
            $("#containerAñadirProductos").html(filas);
            $("#idProveedor").val(idProveedor);
        }
    });

  $("#añadirProductoProveedor").modal("show");
}



function buscarProd(arg){
  var filas=document.getElementsByName("resultadoProd");
  if(arg == "code")
  {
    for(var i=0; i<filas.length; i++)
    {
      if(!filas[i].childNodes[0].innerText.includes($("#codigoProd").val()))
        filas[i].style.display="none";
      else
        filas[i].style.display="table-row";
    }
  }
  else if (arg == "name")
  {
    for(var i=0; i<filas.length; i++)
    {
      if(!filas[i].childNodes[1].innerText.includes($("#nombreProd").val()))
        filas[i].style.display="none";
      else
        filas[i].style.display="table-row";
    }
  }
}

function guardarProductoXProveedor(idProveedor){
  var productos=[];
  var filas=document.getElementsByName("resultadoProd");
  for(var i=0; i<filas.length; i++)
  {
    if(filas[i].childNodes[2].childNodes[0].checked)
      productos.push(filas[i].childNodes[2].childNodes[0].value);
  } //rescato los productos con el checkbox activo
  if(productos.length>0)
    $.ajax({
        type: "POST",cache: false,   dataType:'json', data: {"t":"añadirProductosProveedor","idProveedor":idProveedor ,"productos":productos},
        url: "./api.php", success: function(datos,texto,jqXHR){
              //alert("ECHO");
              swal("Buen trabajo!", "Se han agregado los productos al proveedor seleccionado.", "success");
            }
      });
}