$.fn.initPanelProductos = function () {

  $.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"traerTodosProductos"},
    url: "./api.php", success: function(datos,texto,jqXHR){
      $("#panelBusqueda").empty();
      $("#paginationContenedor").empty();

      $("#panelBusqueda").append(
          '<div class="col-lg-8" style="margin-left: 0%"><div class="input-group"><input type="text" class="form-control" id="busBuscarProducto" placeholder="Producto...." onkeyup="cargarResultadoXclase(\'busBuscarProducto\',\'resultadoProductos\')"><span class="input-group-btn"><button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalNuevoProducto" style="background-color: #2980B9; color: #ECF0F1">Buscar</button></span></div></div><div class="col-lg-4"><button class="btn btn-block btn-primary" name="botonNuevo" id="botonNuevoProducto" onclick="vaciarFormularios(\'productos\')" data-toggle="modal"  data-target="#modalNuevoProducto" style="background-color: #3498DB; border-color: #3498DB; color: #ECF0F1;"><span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 5%"></span><span class="glyphicon glyphicon-open-file" aria-hidden="true"></span></button></div>'
      );

      $("#panelContenedor").empty();
      //Accedemos a la informacion como datos.pacientes[<numero>].<atributo>



      var htmlPagination = '';
      var html= '';
      var cantTotal=0;
      for(var i = 0; i < datos.productos.length; i++){
        if(datos.productos[i].borrado != 1){
            cantTotal++;
             //console.log("CantTotal: "+cantTotal);
        }
      }
  //------ Me fijo si voy a necesitar la navegacion lateral

      if(cantTotal > 45){
          htmlPagination += '<li id="previous"><a onclick="cambiarPagination(\'previous\',\'paginaProductos\',\'linkProductos\',\'resultadoProductos\')" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
          htmlPagination += '<li id="paginaProductos0" class="paginaProductos active"><a id="linkProductos0" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination(\'0\',\'paginaProductos\',\'linkProductos\',\'resultadoProductos\')">1</a></li>';
      }else{
          htmlPagination += '<li><a id="linkProductos0" data-toggle="tab" class="paginaProductos active" aria-expanded="true" onclick="cambiarPagination(\'0\',\'paginaProductos\',\'linkProductos\',\'resultadoProductos\')">1</a></li>';
      }

      //---- Esta variable lleva la cuenta de los elementos que se mostraran, es para que no me queden paginas vacias que vendrian ser de los productos eliminador
      var noBorrados = 0;
      var cantPag= 1;

      for(var i = 0; i < datos.productos.length; i++){
        if(datos.productos[i].borrado != 1){
                  //--- Si pasan 5 reportes creo una lista
                    if((noBorrados%5 == 0)&&(noBorrados!=0)){
                            //--- Agrego una pagina nueva en el pagination
                            if((cantTotal>45)&&(noBorrados == 40)&&(noBorrados != cantTotal -1)){                                      
                                 htmlPagination += '<li><a href="#">...</a></li>';
                            }
                            if((noBorrados>=40)&&(Math.ceil(noBorrados/5)!=Math.ceil(cantTotal/5-1))&&(cantTotal/5-1!=cantPag)){                             
                                cantPag++;
                                htmlPagination += '<li id="paginaProductos'+Math.trunc(noBorrados/5)+'" class="paginaProductos hidden"><a id="linkProductos'+Math.trunc(noBorrados/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination('+Math.trunc(noBorrados/5)+',\'paginaProductos\',\'linkProductos\',\'resultadoProductos\')">'+Math.trunc(noBorrados/5 +1) +'</a></li>';
                            }else{     
                                cantPag++;                                   
                                htmlPagination += '<li id="paginaProductos'+Math.trunc(noBorrados/5)+'" class="paginaProductos"><a id="linkProdcutos'+Math.trunc(noBorrados/5)+'" data-toggle="tab" aria-expanded="true" onclick="cambiarPagination('+Math.trunc(noBorrados/5)+',\'paginaProductos\',\'linkProductos\',\'resultadoProductos\')">'+Math.trunc(noBorrados/5 +1) +'</a></li>';              
                            }      
                            //-- Agrego la lista al div contenedor
                            $("#listaProductos").html(html);                            
                    }
            var popUpEliminar='swal({title: "¿Estas seguro?",text: "Un producto eliminado no podra ser recuperado.",type: "warning",showCancelButton: true,confirmButtonClass: "btn-danger",confirmButtonText: "Si, quiero eliminarlo!",closeOnConfirm: false},function(){swal("Eliminado!", "El producto ha sido eliminado correctamente.", "success");eliminarProducto('+datos.productos[i].id+')});';
            var botonEliminar='<button type="button" onclick=\''+popUpEliminar+'\' class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #3498DB">Eliminar</button>'
            var botonModificar = '<button type="button" onclick="modificarProducto('+datos.productos[i].id+');" class="btn btn-default" style="background-color: #ECF0F1; border-color: #ECF0F1; color: #2980B9" data-toggle="modal" data-target="#modalNuevoProducto">Modificar</button>';
            if(Math.trunc(noBorrados/5)==0)
              html+='<li name="resultadoProductos" class="resultadoProductos'+Math.trunc(noBorrados/5)+' list-group-item row" style="display: block;"><div class="col-md-8" style="padding-left: 2%; padding-right:2%"><p id='+ datos.productos[i].nombre+ '><b>Nombre:</b>  ' +datos.productos[i].nombre + '</p></div><div class="col-md-10"><div class="col-md-5"><p><b>Precio de compra:</b> '+ datos.productos[i].preciocompra+' </p></div><div class="col-md-5" ><p><b>Precio de venta:</b> '+ datos.productos[i].precioventa+'</p></div></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Stock actual:</b> '+ datos.productos[i].cantidadactual+' </p></div>'+botonModificar+botonEliminar+'</li>' ; 
            else
              html+='<li name="resultadoProductos"  class="resultadoProductos'+Math.trunc(noBorrados/5)+' list-group-item row" style="display: none;"><div class="col-md-8" style="padding-left: 2%; padding-right:2%"><p id='+ datos.productos[i].nombre+ '><b>Nombre:</b>  ' +datos.productos[i].nombre + '</p></div><div class="col-md-10"><div class="col-md-5"><p><b>Precio de compra:</b> '+ datos.productos[i].preciocompra+' </p></div><div class="col-md-5" ><p><b>Precio de venta:</b> '+ datos.productos[i].precioventa+'</p></div></div><div class="col-md-8" style=" padding-left: 2%; padding-right:2%"><p><b>Stock actual:</b> '+ datos.productos[i].cantidadactual+' </p></div>'+botonModificar+botonEliminar+'</li>' ; 
            noBorrados++;
        }
      }

    if(cantTotal>45){
      htmlPagination += '<li id="next"><a onclick="cambiarPagination(\'next\',\'paginaProductos\',\'linkProductos\',\'resultadoProductos\')" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
    }

    if(cantTotal == 0){
        html = '<ul><li data-toggle="collapse" data-parent="#listaProductos" name="" class="list-group-item row"><h3>No se encontraron resultados</h3></li>';
        html += '</ul>';
    }

    $("#paginationContenedor").html(htmlPagination);
    $("#panelContenedor").html(html);
      }
  }); 
}; 





//-------------------------------- Control de los campos de modal de la carga de productos ---------------------------
function agregarProducto(id = null){
      var contenedor = document.getElementById('contenedorProductos');
      var productos = document.getElementsByClassName('Producto');
      if(productos.length > 1){
        var ultimoId = productos[productos.length-1].title *1;
        var ultimoId = ultimoId + 1;
      }else
        var ultimoId = 2;
      var ultimoAumento = 34 * productos.length;
      var html = '<tr class="Producto" title=\''+ultimoId+'\'>';
      html += '<td align="center"><input id="nombreProducto'+ultimoId +'" type="text" class="form-control" placeholder="Nombre" aria-describedby="basic-addon1"></td>';
      html += '<td align="center"><select class="form-control" id="tipoVenta'+ultimoId+'" onchange="ajustarTipoVenta(\''+ultimoId+'\')" style="width:145%" name="tipoVenta'+ultimoId+'[]"><option value="0">Unidades</option><option value="1">Peso ($)</option></select> </td>';
      html += '<td><input id="cantidadProducto'+ultimoId +'" onchange="calcularPrecioVenta(\'cant\',\''+ultimoId+'\')" type="number" style="width: 85%; float: right;" step="1" class="form-control" placeholder="Stock Actual" aria-describedby="basic-addon1"  value="0"></td>';
      html += '<td><input id="precioCompra'+ultimoId +'" onchange="calcularPrecioVenta(\'pc\',\''+ultimoId+'\')" type="number" step="1" class="form-control" placeholder="Precio de la compra del producto" aria-describedby="basic-addon1"  value="0"></td>';
      html += '<td><input id="porcentajeVenta'+ultimoId +'" onchange="calcularPrecioVenta(\'com\',\''+ultimoId+'\')" type="number" step="1" class="form-control" placeholder="Comisión" aria-describedby="basic-addon1"  value="0"></td>';
      html += '<td><input id="precioProducto'+ultimoId +'" onchange="calcularPrecioVenta(\'pv\',\''+ultimoId+'\')" type="number" step="0,01" class="form-control" placeholder="Precio de venta" aria-describedby="basic-addon1"></td>';
      html += '<td><button type="button" class="btn btn-default" style="position:fixed; !important; !important;" onclick="eliminarCampoProducto('+ultimoId+');"><span class="glyphicon glyphicon-remove" style="color: #E74C3C" aria-hidden="true"></span></button></td>'; 
      html += '</tr>';

      
      $('#contenedorProductos').append(html); 

}

function eliminarCampoProducto(id){
      var productos = document.getElementsByClassName('Producto');
      for(var i=0;i < productos.length; i++)
        if(id == productos[i].title)
          productos[i].outerHTML  = '';

}

//----------- Calcula el precio de la venta o el porcentaje de comision, se le pasa el elemente que lo genero pc= precio compra, pv= precio venta, com= porcentaje de la comision.
function calcularPrecioVenta(generador,element){
  var preciocompra = document.getElementById('precioCompra' + element).value;
  var comision = document.getElementById('porcentajeVenta' + element).value; 
  var precioventa = document.getElementById('precioProducto'+ element).value;
  var cantidad = document.getElementById('cantidadProducto'+ element).value;
  var tipoventa = document.getElementById('tipoVenta'+ element).value;

  if(tipoventa==1)
  {    
    switch(generador){
        case 'com':
                    if(cantidad != 0){
                        document.getElementById('cantidadProducto'+ element).value = (cantidad * (comision/100) + cantidad*1).toFixed(2);
                    }
              break;
        case 'cant':
                    if(comision != 0){
                        document.getElementById('porcentajeVenta'+ element).value = (cantidad * (comision/100) + cantidad*1).toFixed(2);
                    }
              break;
    }
  }
  else
  {
  //----------------- toFixed() funcion para truncar decimales
    switch(generador){
        case 'pc':
                    if(comision!= 0){
                        document.getElementById('precioProducto'+ element).value = (preciocompra * (comision/100) + preciocompra*1).toFixed(2);
                    }
              break;
        case 'com':
                    if(preciocompra != 0){
                        document.getElementById('precioProducto'+ element).value = (preciocompra * (comision/100) + preciocompra*1).toFixed(2);
                    }
              break;

        case 'pv':
                    if(preciocompra != 0){
                      document.getElementById('porcentajeVenta'+ element).value = ((precioventa*100)/(preciocompra*1) - 100).toFixed(2);
                    }

              break;
    }

  }
}

//----------- Ajustar tipo de venta
function ajustarTipoVenta(element){
  var preciocompra = document.getElementById('precioCompra' + element);
  var comision = document.getElementById('porcentajeVenta' + element).value; 
  var precioventa = document.getElementById('precioProducto'+ element);
  var cantidad = document.getElementById('cantidadProducto'+ element).value;
  var tipoventa = document.getElementById('tipoVenta'+ element).value;

  if(tipoventa==1)
  {    
    preciocompra.value=1;
    preciocompra.type='hidden';
    precioventa.value=1;
    precioventa.type='hidden';
  }
  else
  {
    preciocompra.value=1;
    preciocompra.type='number';
    precioventa.value=1;
    precioventa.type='number';
  }
}


//---------------------------------------- ABM productos  -------------------------------

function guardarProductos(){
    var error=0;
    $("#error").hide();
    $("#error").empty();
    $("#error").append('<b>Por favor mire los siguiente errores</b></br>');



    var productos = document.getElementsByClassName('Producto');
    var arrayP = new Array();
    var arrayNombre = new Array();
    var arrayTipoVenta = new Array();
    var arrayPrecioCompra = new Array();
    var arrayporcentajeVenta = new Array();
    var arrayCantidad = new Array();
    var arrayPrecio = new Array();

    console.log(productos.length);

  
    for(var i=1; i<=productos.length;i++){

        arrayNombre[i-1] = document.getElementById('nombreProducto'+i).value;
        arrayTipoVenta[i-1] = document.getElementById('tipoVenta'+i).value;
        arrayPrecioCompra[i-1] = document.getElementById('precioCompra'+i).value;
        arrayporcentajeVenta[i-1] = document.getElementById('porcentajeVenta'+i).value;
        arrayCantidad[i-1] = document.getElementById('cantidadProducto'+i).value;
        arrayPrecio[i-1] = document.getElementById('precioProducto'+i).value;

        if(document.getElementById('nombreProducto'+i).value == ''){
          error=1;
           $("#error").append('Por favor completar el campo "Nombre" del producto de la linea '+ i +'</br>');
        }
        if(document.getElementById('precioProducto'+i).value == ''){
          error=1;
           $("#error").append('Por favor completar el campo "Precio" del producto de la linea '+ i +'</br>');
        }
    }



        if(error==0)
        $.ajax({
                type: "POST",cache: false,   dataType:'json', data: {"t":"guardarProductos","idProducto":$('#idProducto').val(), "arrayNombre":JSON.stringify(arrayNombre), "arrayTipoVenta":JSON.stringify(arrayTipoVenta),"arrayporcentajeVenta":JSON.stringify(arrayporcentajeVenta),"arrayPrecioCompra":JSON.stringify(arrayPrecioCompra), "arrayCantidad":JSON.stringify(arrayCantidad),"arrayPrecio":JSON.stringify(arrayPrecio)},
                url: "./api.php", success: function(datos,texto,jqXHR){
                  if(datos.err == 0)
                  {
                        if($("#idPaciente").val() != '')
                          swal("Buen trabajo!", "El producto ha sido modificado correctamente!", "success");
                        else
                          swal("Buen trabajo!", "El producto ha sido agregado correctamente!", "success");

                        $("#botonNuevoProducto").click();
                        vaciarFormularios("productos");
                        $().initPanelProductos();
                        $("#error").empty();
                        $("#error").hide();

                  }
                  else if(datos.err == 1)
                  {
                    $("#error").empty();
                    $("#error").append('<b>Por favor mire los siguiente errores</b></br>');
                    $("#error").append(datos.txerr);
                    $("#error").show();


                  }
                }
        });
      else
        $("#error").show();

}

function modificarProducto(id){
        $.ajax({
          type: "POST",cache: false,   dataType:'json', data: {"t":"traerProducto","idProducto":id},
          url: "./api.php", success: function(datos,texto,jqXHR){
                  $(document).ready(function(){//$("#botonNuevoProducto").click();
                          console.log("Entro en modificar producto"+datos.producto[0].tipoventa);
                          console.log("Entro en modificar producto");
                          vaciarFormularios("productos");
                          $("#tituloModal").text("Modificar Producto");
                          //--- Quito el boton para agregar mas productos -----
                          $("#botonAgregarProducto").empty();
                          //-------------------------
                          //--- Me fijo si debo ocultar algun campo
                          if(datos.producto[0].tipoventa==0){
                                document.getElementById('precioCompra1').type = "number";
                                $("#precioCompra1").val(datos.producto[0].preciocompra);
                                document.getElementById('porcentajeVenta1').type = "number";
                                $("#porcentajeVenta1").val(datos.producto[0].comision);
                          }else{
                              document.getElementById('precioCompra1').type = "hidden";
                              document.getElementById('porcentajeVenta1').type = "number";                          
                          }

                          $("#idProducto").val(datos.producto[0].id);
                          $("#nombreProducto1").val(datos.producto[0].nombre);
                          document.getElementById("tipoVenta1")[datos.producto[0].tipoventa].selected = true;

                          $("#cantidadProducto1").val(datos.producto[0].cantidadactual);
                          $("#precioProducto1").val(datos.producto[0].precioventa);

                          ajustarTipoVenta(1);
                          //$().initPanelProductos();

                  });
            }
        });
        
}


function eliminarProducto(id){
        $.ajax({
          type: "POST",cache: false,   dataType:'json', data: {"t":"eliminarProducto","idProducto":id},
          url: "./api.php", success: function(datos,texto,jqXHR){

            }
        });
        $().initPanelProductos();
}
