function initMostrarTerminalCompra(idProveedor){  
$('#idProveedor').val(idProveedor);
$.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"initTerminalCompra","idProveedor": idProveedor},
    url: "./api.php", success: function(datos,texto,jqXHR){
      var html='';
       //CargamosProductos
       for(var i=0;i<datos.productos.length;i++)
       {
        html+='<tr title=\'tabla8producto'+datos.productos[i].id+'\'>';
        html+='<td style="text-align: center;vertical-align: middle;">'+datos.productos[i].id+'</td>'; 
        html+='<td style="text-align: center;vertical-align: middle;">'+datos.productos[i].nombre+'</td>'; 
        html+='<td style="text-align: center;vertical-align: middle;width:20%">'; 
        if(datos.productos[i].tipoventa==1) 
          html+='$ ' +datos.productos[i].cantidadactual; 
        else
          html+=datos.productos[i].cantidadactual + ' unidades'; 
        html+='<td style="text-align: center;vertical-align: middle;">'+datos.productos[i].preciocompra+'</td>'; 
        html+='<td style="text-align: center;vertical-align: middle;"><div class="col-lg-12">'; 
        var boton='<button onclick="agregarProducto('+datos.productos[i].id+');" class="btn btn-block btn-primary"  style="background-color: #8BC34A; border-color: #7CB342; color: #ECF0F1"><span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 5%"></span></span></button>';
        html+=boton;
        html+='</td>'; 
        html+='</tr>';      
       }

       $('#containerproductos').html(html);  
       $('#containerProductosVenta').html('');  

      }
  });

$.ajax({
    type: "POST",cache: false,   dataType:'json', data: {"t":"traerUsuarioActual"},
    url: "./api.php", success: function(datos,texto,jqXHR){
      var html=datos.usuario[0].apellido+', '+datos.usuario[0].nombre;

       $('#nombreapellidosusuario').html(html);  

      }
  });


actualizarTotal();
};

//BUSCADORES
$("#filtroCodigoProducto").change(function(){
  filtrarTabla('productos','filtroCodigoProducto',0);
});
$("#filtroNombreProducto").change(function(){
  filtrarTabla('productos','filtroNombreProducto',1);
});

function volverTerminalAdministrador(){

    swal("Genial!", "Estamos volviendo a la terminal de Administador.", "success");
    $("#mainPanel").load("vista/terminalAdministrador/terminalAdministrador.html");
    $("#menuMain").load("vista/terminalAdministrador/menuAdministrador.html");
}

function actualizarTotal(){
  var total=0;
  var filas = $('#containerProductosVenta tr');
    for(var i=0;i<filas.length;i++)
      total+=($('#cantidadProductoVenta'+filas[i].title).val()*1) * ($('#precioCompraProducto'+filas[i].title).val()*1);
   $('#totalVenta').html('<b>Total: $ '+total+'</b>');   
}


function agregarProducto(id){
  var filas = $('#containerproductos tr');
  var html=''; 
    for(var i=0;i<filas.length;i++)
    {
      console.log(filas[i].title);
      if(filas[i].title=='tablaproducto'+id){          
        var content=filas;
        html+='<tr title='+id+'><input type="hidden" value="'+content[i].childNodes[0].innerHTML+'" id="productosvendidos'+id+'"name="productosvendidos" >';
        html+='<td style="text-align: center;vertical-align: middle;">'+content[i].childNodes[1].innerHTML+'</td>';
        html+='<td style="text-align: center;vertical-align: middle;width:20%"><div class="input-group col-md-12">';
        var input = '' ;
        var unidad=content[i].childNodes[2].innerHTML.split(" ")[0];
        console.log(content[i].childNodes[2].innerHTML.split(" ")[1]);
        if(unidad=='$')
          input+='<input type="number" title="1" id="cantidadProductoVenta'+id+'" onchange="actualizarTotal();" name="cantidadProductoVenta" class="form-control" value=1 step="0.01" min="0.01" ><span class="input-group-addon">Pesos.</span></div>'; 
        else
          input+='<input type="number" title="1" id="cantidadProductoVenta'+id+'" onchange="actualizarTotal();" name="cantidadProductoVenta" class="form-control" value=1 step=1 min=1 ><span class="input-group-addon">Uni.</span></div>';
        html+=input;
        html+='</td><td style="text-align: center;vertical-align: middle;">';
        if(unidad=='$')
          input='<input type="number" title="1" id="precioCompraProducto'+id+'" onchange="actualizarTotal();" name="precioCompraProducto" readonly class="form-control" value=1 step="0.01" min="0.01" ></div>'; 
        else
          input='<input type="number" title="1" id="precioCompraProducto'+id+'" onchange="actualizarTotal();" name="precioCompraProducto" class="form-control" value='+content[i].childNodes[3].innerHTML+' step=1 min=1 ></div>';
        html+=input;
        html+='</td><td style="text-align: center;vertical-align: middle;">';
        var boton='<button onclick="eliminartablaventaproducto('+id+')" class="btn btn-block btn-primary"  style="background-color: #D32F2F; border-color: #C62828; color: #ECF0F1;"><span class="glyphicon glyphicon-remove" aria-hidden="true" style="margin-right: 5%"></span></button>';
        html+=boton;
        html+='</td>';
        html+='</tr>';
        
        filas[i].outerHTML='';
      }
      
    }


   $('#containerProductosVenta').append(html); 
   actualizarTotal();
 }


function eliminartablaventaproducto(id){
  var filas = $('#containerProductosVenta tr');
  var html='';
  for(var i=0;i<filas.length;i++)
  {
    console.log(filas[i].title);
    if(filas[i].title==id){   
      html+='<tr title=\'tablaproducto'+id+'\'>';
      html+='<td style="text-align: center;vertical-align: middle;">'+$('#productosvendidos'+id).val()+'</td>'; 
      html+='<td style="text-align: center;vertical-align: middle;">'+filas[i].childNodes[1].innerHTML+'</td>'; 
      html+='<td style="text-align: center;vertical-align: middle;width:20%">'; 
      html+=$('#cantidadProductoVenta'+id).attr('max');
      html+='<td style="text-align: center;vertical-align: middle;">'+$('#precioCompraProducto'+id).val()+'</td>'; 
      html+='<td style="text-align: center;vertical-align: middle;"><div class="col-lg-12">'; 
      var boton='<button onclick="agregarProducto('+id+');" class="btn btn-block btn-primary"  style="background-color: #8BC34A; border-color: #7CB342; color: #ECF0F1"><span class="glyphicon glyphicon-plus" aria-hidden="true" style="margin-right: 5%"></span></span></button>';
      html+=boton;
      html+='</td>'; 
      html+='</tr>';   
        
      filas[i].outerHTML='';

   $('#containerproductos').append(html); 
  }
}

actualizarTotal();
}


function mostrarModalCerrarVenta(){
  var filas = $('#containerProductosVenta tr');
  var html='';
  var error=0;

  if(filas.length==0)
    swal("Tenemos un problema!", "La compra no tiene ningun producto.")
  else
  {
    for(var i=0;i<filas.length;i++)
    {
      var id=filas[i].title;
      var nombre=filas[i].childNodes[1].innerHTML;
      var cantidad=$('#cantidadProductoVenta'+id).val()*1;
      if(cantidad<=0 )
        error=1;

      if(cantidad==1)
        html+=cantidad+' unidad de '+nombre+'</br>';
      else
        html+=cantidad+' unidades de '+nombre+'</br>';
    }

    if(error==1)
    {
    swal({
      title: "Detectamos un error!",
      html:true,
      text: "Revisa la cantidad de productos que compraste porque hay una cantidad que esta mal ingresada."  ,
      type: "warning"
    });

    }
    else
    {
    swal({
      title: "¿Estas seguro?",
      html:true,
      text: "Revisa los productos que compraste:</br><div style='font-size:14px;'>"+html+'</div>',
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Cerrar compra",
      confirmCancelText: "Cancelar",
      closeOnConfirm: false
    },
    function(){
    var productos = document.getElementsByName('productosvendidos');
    var array = new Array();
    var cantidades = new Array();
    var precios = new Array();
    var idProveedor = $('#idProveedor').val();
    for(var i=0; i<productos.length;i++){
        array[i] = productos[i].value;
        cantidades[i] = $('#cantidadProductoVenta'+productos[i].value).val();
        precios[i] = $('#precioCompraProducto'+productos[i].value).val();
    }
      $.ajax({
        type: "POST",cache: false,   dataType:'json', data: {"t":"cerrarCompra","productos":JSON.stringify(array),"cantidades":JSON.stringify(cantidades),"precios":JSON.stringify(precios),"idProveedor":idProveedor},
        url: "./api.php", success: function(datos,texto,jqXHR){

            if(datos.err==0)
            {
              swal("Genial!", "La compra se registro correctamente.", "success");
              location.reload();
            }
            else
              swal("Upps!", "La compra no se registro correctamente.", "warning");

          }
      });
    }); 

    }   
  }

  $('#totalVenta').html('<b>Total: $ 0</b>');  
}



//filtrar table filtra la tabla idtable, tomando el texto de idinput, y con referenciaa a la fila (fila) de la tabla
function filtrarTabla(idtabla, idinput, fila) {
    var tableReg = $('#' + idtabla + ' tbody tr');
    var searchText = document.getElementById(idinput).value.toLowerCase();
    var cellsOfRow = "";
    var found = false;
    var compareWith = "";

    // Recorremos todas las filas con contenido de la tabla
    for (var i = 0; i < tableReg.length; i++) {
        cellsOfRow = tableReg[i].getElementsByTagName('td');
        found = false;
        // Recorremos todas las celdas
        for (var j = 0; j < cellsOfRow.length && !found; j++) {
            if (j == fila) { //OBLIGO A QUE SEA LA CELDA DE LA FILA
                compareWith = cellsOfRow[j].innerHTML.toLowerCase();
                // Buscamos el texto en el contenido de la celda
                if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)) {
                    found = true;
                }

            }
        }
        if (found) {
            tableReg[i].style.display = '';
        } else {
            // si no ha encontrado ninguna coincidencia, esconde la
            // fila de la tabla
            tableReg[i].style.display = 'none';
        }
    }
}

function buscarProd(arg){
  var filas=document.getElementsByName("resultadoProd");
  if(arg == "code")
  {
    for(var i=0; i<filas.length; i++)
    {
      if(!filas[i].childNodes[0].innerText.includes($("#filtroCodigoProducto").val()))
        filas[i].style.display="none";
      else
        filas[i].style.display="table-row";
    }
  }
  else if (arg == "name")
  {
    for(var i=0; i<filas.length; i++)
    {
      if(!filas[i].childNodes[1].innerText.includes($("#filtroNombreProducto").val()))
        filas[i].style.display="none";
      else
        filas[i].style.display="table-row";
    }
  }
}