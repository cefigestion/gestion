<?php
require_once('Clases/Persistencia/ConexionBD.php');

class Accion{
	private $id;
	private $idUsuario;
	private $fechahora;
	private $tipoaccion;

	public function __construct(){
		if(isset($_POST['idAccion']))
			$this->id = $_POST['idAccion'];
		if(isset($_POST['idUsuario']))
			$this->idUsuario = $_POST['idUsuario'];
		if(isset($_POST['fechahoraAccion']))
			$this->fechahora = $_POST['fechahoraAccion'];
		if(isset($_POST['tipoAccion']))
			$this->tipoaccion = $_POST['tipoAccion'];
	}

	public function setIdUsuario($newIdUsuario){
		$this->idUsuario = $newIdUsuario;
	}

	public function setAccion($newAccion){
		$this->tipoaccion = $newAccion;
	}

	public function setFechaHora($newFechaHora){
		$this->fechahora = $newFechaHora;
	}


	public function guardar(){
		//aca se ve si el id es nulo, se hace la carga como si fuera nuevo INSERT, si el id es distinto de nulo lo que hay que hacer es un UPDATE
		if($this->id == null){
			//die($this->fechahora);
			$sql = "INSERT INTO acciones(id,idusuario,fechahora,tipoaccion) VALUES (NULL,$this->idUsuario,'{$this->fechahora}' ,'{$this->tipoaccion}')";
			
			ConexionBD::obtenerInstancia()->consultar($sql);

		} else {
/*
			$sql = "UPDATE productos SET nombre='{$this->nombre}' , cantidadactual=$this->cantidad , precioventa=$this->precio , comision=$this->comision , precioncompra=$this->precioCompra WHERE id='{$this->id}'";
			ConexionBD::obtenerInstancia()->consultar($sql);*/
		}

	}

	public static function traerAccesos($idUser){
		//SELECT fechahora FROM acciones WHERE idusuario=1 and tipoaccion="Ingreso al sistema" ORDER by id
		$sql = "SELECT fechahora FROM acciones WHERE idusuario=".$idUser ." and tipoaccion='Ingreso al sistema' order by id DESC";
		$respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $respuesta;
	}

	public static function accionesXusuario($idUsuario){
		$sql="SELECT * FROM acciones WHERE idusuario=".$idUsuario . " ORDER BY id DESC"; //BY fechahora DESC 
		//die($sql);
		$rta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $rta;
	}
}
?>