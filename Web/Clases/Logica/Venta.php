<?php

require_once('Clases/Persistencia/ConexionBD.php');

class Venta
{

	private $id=null;
	private $total=null;
	private $fecha=null;
	private $hora=null;
	private $cancelar=0;
	private $idCaja=null;

	public function __construct($id = null, $idCaja = null)
	{

	}

	public function setId($data){
		$this->id=$data;
	}

	public function getId(){
		return $this->id;
	}

	public function setIdCaja($data){
		$this->idCaja=$data;
	}

	public function getIdCaja(){
		return $this->idCaja;
	}

	public function setTotal($data){
		$this->total=$data;
	}

	public function getTotal(){
		return $this->total;
	}

	public function setFecha($data){
		$this->fecha=$data;
	}

	public function getFecha(){
		return $this->fecha;
	}

	public function setHora($data){
		$this->hora=$data;
	}

	public function getHora(){
		return $this->hora;
	}

	public function setCancelar($data){
		$this->cancelar=$data;
	}

	public function getCancelar(){
		return $this->cancelar;
	}

	public function guardar(){
		if($this->id == null){
			$con=ConexionBD::obtenerInstancia();
			$sql="INSERT INTO ventas VALUES (null,$this->total,'$this->fecha','$this->hora',$this->cancelar,$this->idCaja)";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
			$sql="SELECT MAX(id) as id FROM ventas";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
	      	$this->id=armarArrayCon($registro)[0]['id'];
		}
		else{
		//MODIFICAR	
		}
	}

	//Inicializa fecha y hora a la actual
	public function inicializar(){
		$this->setFecha(date('Y-m-d'));
		$this->setHora(date('H:i:s'));
	}

	public static function traerProductosDeVenta($idventa){
		$sql="SELECT nombre, cantidad, productosventas.precioventa as precio FROM productos, productosventas WHERE productosventas.idventas=$idventa AND productosventas.idproductos=productos.id";
		$rta=ConexionBD::obtenerInstancia()->consultar($sql);
	      	//$this->id=armarArrayCon($rta)[0]['id'];
		return $rta;
	}

	public static function trearProximaVenta(){
		$sql="SELECT MAX(id) as id FROM ventas";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
      	$id=armarArrayCon($registro)[0]['id']+1;
		return $id;
	}
}

?>