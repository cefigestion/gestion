<?php

require_once('Clases/Persistencia/ConexionBD.php');

class Usuario{
	private $id;
	private $nombre;
	private $apellido;
	private $dni;
	private $pass;
	private $tipo;
	private $bloqueado;

	public function __construct(){
		if(isset($_POST["idUsuario"]))
			$this->id = $_POST['idUsuario'];
		if(isset($_POST["nombreUsuario"]))
			$this->nombre = $_POST['nombreUsuario'];
		if(isset($_POST["apellidoUsuario"]))
			$this->apellido = $_POST['apellidoUsuario'];
		if(isset($_POST["dniUsuario"]))
			$this->dni = $_POST['dniUsuario'];
		if(isset($_POST["passUsuario"]))
			$this->pass = $_POST['passUsuario'];
		if(isset($_POST["tipoUsuario"]))
			$this->tipo = $_POST['tipoUsuario'];
		if(isset($_POST["estadoUsuario"]))
			$this->bloqueado = $_POST['estadoUsuario'];
		else
			$this->bloqueado = 0;
	}

//------------ Set de los atributos --------
	public function setIdUsuario($newId){
		$this->id = $newId;
	}


	public function getTipoUsuario(){
		if($this->tipo == 0)
			$rta = "Administrador";
		else
			$rta = "Vendedor";
		return $rta;
	}

//-------------- Otros metodos -------------------
	public function actualizarObservacion($text){
		$sql = "UPDATE usuarios SET observacion='". $text . "' WHERE id= " . $this->id;
		//die($sql);
		ConexionBD::obtenerInstancia()->consultar($sql);
	}

	public function traerUsuario(){
		$sql = "SELECT * FROM usuarios WHERE id=".$this->id;
	        $respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
	        //die($sql);
	    return $respuesta;

	}

	public static function traerTodos(){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT * FROM usuarios";
        $respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $respuesta;
	} 	

	public function guardar(){
		//aca se ve si el id es nulo, se hace la carga como si fuera nuevo INSERT, si el id es distinto de nulo lo que hay que hacer es un UPDATE
		if($this->id == null){
			$sql = "INSERT INTO usuarios(id,nombre,apellido,dni,pass,tipo,bloqueado) VALUES (NULL,'{$this->nombre}','{$this->apellido}' ,$this->dni, '{$this->pass}' ,$this->tipo, $this->bloqueado)";
			//die($sql);
			ConexionBD::obtenerInstancia()->consultar($sql);

		} else {

			$sql = "UPDATE usuarios SET nombre='{$this->nombre}' , apellido='{$this->apellido}' , dni=$this->dni , pass='{$this->pass}' , tipo=$this->tipo , bloqueado=$this->bloqueado WHERE id='{$this->id}'";
			ConexionBD::obtenerInstancia()->consultar($sql);
		}

	} 

	public function bloquear(){
	//aca se ve si el id es nulo, es error, si el id es distinto de nulo lo que hay que hacer es un DELETE
		if($this->id == null){
			//MOSTRAR ERROR
			die("error");
		} else {
			$sql = "UPDATE usuarios SET bloqueado=1 WHERE id=".$this->id;
			ConexionBD::obtenerInstancia()->consultar($sql);

		}
	}

	public function desbloquear(){
	//aca se ve si el id es nulo, es error, si el id es distinto de nulo lo que hay que hacer es un DELETE
		if($this->id == null){
			//MOSTRAR ERROR
			die("error");
		} else {
			$sql = "UPDATE usuarios SET bloqueado=0 WHERE id=".$this->id;
			ConexionBD::obtenerInstancia()->consultar($sql);

		}		
	}

	public function ultimoAcceso(){
		if($this->id == null)
			die("error");
		else{
			$sql = "SELECT fechahoraapertura WHERE idusuario=".$this->id;
			ConexionBD::obtenerInstancia()->consultar($sql);
		}
	}

	public function traerCajas(){
		$sql = "SELECT * FROM cajas WHERE idusuario=".$this->id;
	        $respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
	        //die($sql);
	    return $respuesta;
	}

	public function getNombreXid($idUser){
		$con=ConexionBD::obtenerInstancia();
		$sql = "SELECT nombre From usuarios Where id=".$idUser;
		$respuesta = $con->consultar($sql);
		return $respuesta;
	}

	public function getApellidoXid($idUser){
		$con=ConexionBD::obtenerInstancia();
		$sql = "SELECT apellido From usuarios Where id=".$idUser;
		$respuesta = $con->consultar($sql);
		return $respuesta;
	}

	public function limpiarAcciones($idUser){
		$hoy=intval(date('d'));
		if($hoy==28)	//Se borran el 28 de cada mes 
		{
			$sql="DELETE FROM acciones WHERE idUsuario=$idUser and STR_TO_DATE(fechahora,'%d/%m/%Y') < CURDATE()";
			ConexionBD::obtenerInstancia()->consultar($sql);
		}
	}

}
?>