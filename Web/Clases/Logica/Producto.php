<?php

require_once('Clases/Persistencia/ConexionBD.php');

class Producto
{
	private $id;
	private $nombre;
	private $tipoVenta;
	private $precioCompra;
	private $comision;
	private $precio;
	private $cantidad;

	public function __construct(){
		if(isset($_POST["idProducto"]))
			$this->id = $_POST['idProducto'];
		if(isset($_POST["tipoVenta"]))
			$this->tipoVenta = $_POST['tipoVenta'];
		if(isset($_POST['nombreProducto']))
			$this->nombre = $_POST['nombreProducto'];
		if(isset($_POST['descripcionProducto']))
			$this->descripcion = $_POST['descripcionProducto'];
		if(isset($_POST['precioProducto']))
			$this->precio = $_POST¨['precioProducto'];
		if(isset($_POST['cantidadProducto']))
			$this->cantidad = $_POST['cantidadProducto'];
	}

//------------- Set de los atributos ------------
	public function setNombreProducto($newNombre){
		$this->nombre = $newNombre;
	}

	public function setTipoVenta($newTipoVenta){
		$this->tipoVenta = $newTipoVenta;
	}

	public function setPrecioCompraProducto($newPrecioCompra){
		$this->precioCompra = $newPrecioCompra;
	}

	public function setComisionProducto($newComision){
		$this->comision = $newComision;
	}

	public function setPrecioProducto($newPrecio){
		$this->precio = $newPrecio;
	}

	public function setCantidadProducto($newCantidad){
		$this->cantidad = $newCantidad;
	}

	public function setIdProducto($newId){
		$this->id = $newId;
	}

	public function getNombreXid($idProd){
		$con=ConexionBD::obtenerInstancia();
		$sql = "SELECT nombre From productos Where id=".$idProd;
		$respuesta = $con->consultar($sql);
		return $respuesta;
	}

	//------------ Demas funcion ------------
	public static function traerTodos(){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT * FROM productos";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		return $registro;
	} 

	public static function traerTodosPorProveedor($idProveedor){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT * FROM productos WHERE borrado=0 and productos.id in (SELECT idproducto FROM productosproveedores WHERE $idProveedor = idproveedor )";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		return $registro;
	} 

	public static function traerTodosConStockMayor0(){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT * FROM productos WHERE cantidadactual>0";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		return $registro;
	} 

	public function traerProducto(){
	//aca se ve si el id es nulo, es error, si el id es distinto de nulo lo que hay que hacer es un DELETE

		$sql = "SELECT * FROM productos WHERE id=".$this->id;
	        $respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
	        return $respuesta;
	}


	public function guardar(){
		//aca se ve si el id es nulo, se hace la carga como si fuera nuevo INSERT, si el id es distinto de nulo lo que hay que hacer es un UPDATE
		if($this->id == null){
			//die($this->nombre);

			$sql = "INSERT INTO productos(id,nombre,tipoventa,comision,preciocompra,cantidadactual,precioventa) VALUES (NULL,'{$this->nombre}',$this->tipoVenta,$this->comision ,$this->precioCompra,$this->cantidad,$this->precio)";
			ConexionBD::obtenerInstancia()->consultar($sql);

		} else {

			$sql = "UPDATE productos SET nombre='{$this->nombre}' ,tipoventa=$this->tipoVenta, cantidadactual=$this->cantidad , precioventa=$this->precio , comision=$this->comision , preciocompra=$this->precioCompra WHERE id='{$this->id}'";
			//die($sql);
			ConexionBD::obtenerInstancia()->consultar($sql);
		}

	} 

	public function actualizarCantidad(){
		$sql = "UPDATE productos SET cantidadactual=$this->cantidad WHERE id=".$this->id;
	        $respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
	}

	public function actualizarPrecioCompra(){
		$sql = "UPDATE productos SET preciocompra=$this->precioCompra WHERE id=".$this->id;
	        $respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
	}

	public function eliminar(){
	//aca se ve si el id es nulo, es error, si el id es distinto de nulo lo que hay que hacer es un DELETE
		if($this->id == null){
			//MOSTRAR ERROR
			die("error");
		} else {
			$sql = "UPDATE productos SET borrado=1 WHERE id=".$this->id;
			ConexionBD::obtenerInstancia()->consultar($sql);

		}
	}


}

?>