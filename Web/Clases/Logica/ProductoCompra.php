<?php

require_once('Clases/Persistencia/ConexionBD.php');

class ProductoCompra
{

	private $id=null;
	private $idCompra=null;
	private $idProducto=null;
	private $cantidad=null;
	private $precioCompra=null;

	public function __construct($id = null, $idCompra = null ,$idProducto = null)
	{
		if($id != null){
			$this->id=$id;
		}else if($idCompra != null && $idProducto != null){
			$this->idCompra=$idCompra;
			$this->idProducto=$idProducto;
		}

	}

	public function setIdProducto($data){
		$this->idProducto=$data;
	}

	public function getIdProducto(){
		return $this->idProducto;
	}

	public function setCantidad($data){
		$this->cantidad=$data;
	}

	public function getCantidad(){
		return $this->cantidad;
	}

	public function setPrecioCompra($data){
		$this->precioCompra=$data;
	}

	public function getPrecioCompra(){
		return $this->precioCompra;
	}

	public function guardar(){
		if($this->id == null){
			$con=ConexionBD::obtenerInstancia();
			$sql="INSERT INTO ProductosCompras VALUES (null,$this->cantidad,$this->precioCompra,$this->idProducto,$this->idCompra)";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		}
		else{
		//MODIFICAR	
		}
	}


}

?>
