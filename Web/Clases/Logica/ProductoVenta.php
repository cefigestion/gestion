<?php

require_once('Clases/Persistencia/ConexionBD.php');

class ProductoVenta
{

	private $id=null;
	private $idVenta=null;
	private $idProducto=null;
	private $cantidad=null;
	private $precioVenta=null;

	public function __construct($id = null, $idVenta = null ,$idProducto = null)
	{
		if($id != null){
			$this->id=$id;
		}else if($idVenta != null && $idProducto != null){
			$this->idVenta=$idVenta;
			$this->idProducto=$idProducto;
		}

	}

	public function setIdProducto($data){
		$this->idProducto=$data;
	}

	public function getIdProducto(){
		return $this->idProducto;
	}

	public function setCantidad($data){
		$this->cantidad=$data;
	}

	public function getCantidad(){
		return $this->cantidad;
	}

	public function setPrecioVenta($data){
		$this->precioVenta=$data;
	}

	public function getPrecioVenta(){
		return $this->precioVenta;
	}

	public function guardar(){
		if($this->id == null){
			$con=ConexionBD::obtenerInstancia();
			$sql="INSERT INTO ProductosVentas VALUES (null,$this->cantidad,$this->precioVenta,$this->idVenta,$this->idProducto)";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		}
		else{
		//MODIFICAR	
		}
	}


}

?>
