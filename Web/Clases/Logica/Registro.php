<?php

require_once('Clases/Persistencia/ConexionBD.php');

class Registro{
	private $id;
	private $mensaje;
	private $idcaja1;
	private $idcaja2;


	public function __construct(){
		if(isset($_POST["idRegistro"]))
			$this->id = $_POST['idRegistro'];
		if(isset($_POST["mensajeRegistro"]))
			$this->mensaje = $_POST['mensajeRegistro'];
		if(isset($_POST["idcaja1Registro"]))
			$this->idcaja1 = $_POST['idcaja1Registro'];
		if(isset($_POST["idcaja2Registro"]))
			$this->idcaja2 = $_POST['idcaja2Registro'];
	}

	public function setId($newid){
		$this->id=$newid;
	}

	public static function traerRegistro(){
		$sql="SELECT * FROM registroscajas WHERE id={$this->id}";
		$respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $respuesta;
	}

	public static function traerTodos(){
		$sql="SELECT * FROM registroscajas ORDER BY fecha DESC";
		$respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $respuesta;
	}

}

?>