<?php

//require_once('../../Clases/Persistencia/ConexionBD.php'); USAR ESTO PARA PROBAR EL CAJA.PHP SOLO
require_once('Clases/Persistencia/ConexionBD.php');


class Caja
{
	private $id;
	private $idUsuario;
	private $cerrada;
	private $fechaApertura;
	private $fechaCierre;
	private $montoInicial;
	private $MontoFinal;

	private function __construct(){
		if($_POST['idUsuario']){
			$this->idUsuario = $_POST['idUsuario'];
		}
	}

	public static function hayAbierta(){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT COUNT(*) AS cant FROM cajas WHERE cerrada=0";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
      	$cant=armarArrayCon($registro)[0]['cant'];
		return $cant > 0;
	} 

	public static function abrirCaja($montoInicial,$idUsuario){
		$con=ConexionBD::obtenerInstancia();
		if(!Caja::hayAbierta())
		{
			if(Caja::ultimaCajaCerrada()!=false)
			{
				$ultimaCajaCerrada=Caja::ultimaCajaCerrada();
				$sql="SELECT montofinal FROM Cajas WHERE id=$ultimaCajaCerrada";
		        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		      	$montofinal=armarArrayCon($registro)[0]['montofinal'];
		      	if($montofinal!=$montoInicial){
		      		$idProxCaja=$ultimaCajaCerrada+1;
		      		$fechaHoy=date('Y-m-d H-i-s');
					$sql="INSERT INTO registroscajas (idcaja1,idcaja2,mensaje, fecha) VALUES ($idProxCaja,$ultimaCajaCerrada,'Al abrir la caja se encontro una discrepancia entre el monto final de la caja anterior, y el monto incial de la caja nueva.', '$fechaHoy')";
			        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		      	}

			}
			$fechahoy=date('Y-m-d H-i-s');
			$sql="INSERT cajas (idusuario,fechahoraapertura,montoinicial,cerrada) VALUES ($idUsuario,'$fechahoy',$montoInicial,0)";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
			return true;

		}
		else
			return "Ya hay una caja abierta";
	} 

	public static function cerrarCaja($montoFinal){
		if(Caja::hayAbierta())
		{
			$idCajaAbierta=Caja::idCajaAbierta();
			$sql="SELECT montoinicial FROM Cajas WHERE id=$idCajaAbierta";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
	      	$montoInicial=armarArrayCon($registro)[0]['montoinicial'];

        	$ventas=Caja::traerVentasDeCaja($idCajaAbierta);
        	$retiros=Caja::traerRetirosDeCaja($idCajaAbierta);
      		$ventas=armarArrayCon($ventas);
      		$retiros=armarArrayCon($retiros);

      		$monto=$montoFinal-$montoInicial;

      		$montoretiros=0;
      		$total=0;
      		for($i=0;$i<sizeof($ventas);$i++)
      			$total+=$ventas[$i]['total'];

      		for($i=0;$i<sizeof($retiros);$i++)
      			$montoretiros+=$retiros[$i]['monto'];

      		$monto+=$montoretiros;

      		if($total!=$monto)
      		{
      			$fechaHoy=date('Y-m-d H-i-s');
				$sql="INSERT INTO registroscajas (idcaja1,mensaje, fecha) VALUES ($idCajaAbierta,'Al cierre de la caja se encontro una discrepancia entre el saldo de la caja y el saldo total de ventas.', '$fechaHoy') ";
		        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
      		}

			$fechahoy=date('Y-m-d H-i-s');
			$sql="UPDATE cajas SET montofinal=$montoFinal, cerrada=1, fechahoracierre='$fechahoy' WHERE id=$idCajaAbierta";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);

		}
		else
		return false;
	} 

	public static function ultimaCajaCerrada(){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT COUNT(*) as cant FROM cajas WHERE cerrada=1";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
      	$cant=armarArrayCon($registro)[0]['cant'];
      	if($cant>0)
      	{
			$sql="SELECT MAX(id) as id FROM Cajas WHERE cerrada=1";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
	      	$id=armarArrayCon($registro)[0]['id'];
			return $id;	
      	}
      	else
      		return false;
	}

	public static function cajaAbiertaSegunIdUsuario($idUsuario){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT COUNT(*) as cant FROM cajas WHERE cerrada=0 and idusuario=$idUsuario";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
      	$cant=armarArrayCon($registro)[0]['cant'];
  		return $cant > 0;
	}

	public static function idCajaAbierta(){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT id FROM Cajas WHERE cerrada=0";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
      	$id=armarArrayCon($registro)[0]['id'];
      	return $id;
	}

	public static function traerVentasDeCaja($idCaja){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT * FROM ventas WHERE idcaja=$idCaja";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
        return $registro;
	}

	public static function traerRetirosDeCaja($idCaja){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT * FROM retiros WHERE cajas_id=$idCaja";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
        return $registro;
	}


	public static function traerTodos(){
		$sql="SELECT * FROM cajas ORDER BY fechahoraapertura DESC";
		$rta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $rta;
	}

	public static function traerCaja($idCaja){	//ESTO PUEDE SER REEMPLAZADO POR EL CONSTRUCTOR SI SE TOMAN LOS VALORES CON EL $_POST
		$sql="SELECT * FROM cajas WHERE id=$idCaja";
		$rta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $rta;
	}


	public static function obtenerVendedor($idCaja){
		$sql = "SELECT * FROM usuarios WHERE id IN (SELECT idusuario From cajas WHERE id= ". $idCaja. ")";
		//die($sql);
		$rta = ConexionBD::obtenerInstancia()->consultar($sql);

		return $rta;
	}

}

?>
