<?php

require_once('Clases/Persistencia/ConexionBD.php');

class Compra
{

	private $id=null;
	private $total=null;
	private $fecha=null;
	private $hora=null;
	private $cancelar=0;
	private $idProveedores=null;
	private $idUsuarios=null;
	//private $idCaja=null;

	public function __construct($id = null, $idCaja = null)
	{

	}

	public function setId($data){
		$this->id=$data;
	}

	public function getId(){
		return $this->id;
	}

	/*public function setIdCaja($data){
		$this->idCaja=$data;
	}

	public function getIdCaja(){
		return $this->idCaja;
	}*/

	public function setTotal($data){
		$this->total=$data;
	}

	public function getTotal(){
		return $this->total;
	}

	public function setFecha($data){
		$this->fecha=$data;
	}

	public function getFecha(){
		return $this->fecha;
	}

	public function setHora($data){
		$this->hora=$data;
	}

	public function getHora(){
		return $this->hora;
	}

	public function setIdProveedores($data){
		$this->idProveedores=$data;
	}

	public function getIdProveedores(){
		return $this->idProveedores;
	}

	public function setIdUsuarios($data){
		$this->idUsuarios=$data;
	}

	public function getIdUsuarios(){
		return $this->idUsuarios;
	}

	public function setCancelar($data){
		$this->cancelar=$data;
	}

	public function getCancelar(){
		return $this->cancelar;
	}

	public function guardar(){
		if($this->id == null){
			$con=ConexionBD::obtenerInstancia();
			$sql="INSERT INTO compras VALUES (null,'$this->fecha',$this->total,$this->idProveedores,$this->idUsuarios)";
			//die($sql);
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
			$sql="SELECT MAX(id) as id FROM compras";
	        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
	      	$this->id=armarArrayCon($registro)[0]['id'];
		}
		else{
		//MODIFICAR	
		}
	} 

	//Inicializa fecha y hora a la actual
	public function inicializar(){
		$this->setFecha(date('Y-m-d'));
		$this->setHora(date('H:i:s'));
	}

	public static function traerProductosDeCompra($idcompra){
		$sql="SELECT idproducto,nombre, cantidad, productoscompras.preciocompra as precio FROM productos, productoscompras WHERE productoscompras.idcompra=$idcompra AND productoscompras.idproducto=productos.id";
		//die($sql);
		$rta=ConexionBD::obtenerInstancia()->consultar($sql);
	      	//$this->id=armarArrayCon($rta)[0]['id'];
		return $rta;
	}

	public static function comprasXusuario($idUsuario){
		$sql = "SELECT * FROM compras WHERE idusuarios = '". $idUsuario ."'";
		//die($sql);
		$respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $respuesta;
	}

	public static function traerCompra($idCompra){	//ESTO PUEDE SER REEMPLAZADO POR EL CONSTRUCTOR SI SE TOMAN LOS VALORES CON EL $_POST
		$sql="SELECT * FROM compras WHERE id=$idCompra";
		$rta=ConexionBD::obtenerInstancia()->consultar($sql);
		return $rta;
	}
}

?>