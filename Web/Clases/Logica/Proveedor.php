<?php

require_once('Clases/Persistencia/ConexionBD.php');

class Proveedor
{
	private $id;
	private $razonSocial;
	private $telefono;
	private $email;
	private $ciut;

	public function __construct(){
		if(isset($_POST["idProveedor"]))
			$this->id = $_POST['idProveedor'];
		if(isset($_POST["razonSocialProveedor"]))
			$this->razonSocial = $_POST['razonSocialProveedor'];
		if(isset($_POST['telefonoProveedor']))
			$this->telefono = $_POST['telefonoProveedor'];
		if(isset($_POST['emailProveedor']))
			$this->email = $_POST['emailProveedor'];
		if(isset($_POST['cuitProveedor']))
			$this->cuit = $_POST['cuitProveedor'];

	}

//------------- Set de los atributos ------------
	public function setRazonSocialProducto($newRazonSocial){
		$this->razonSocial = $newRazonSocial;
	}

	public function setTelefono($newTelefono){
		$this->telefono = $newTelefono;
	}

	public function setEmail($newEmail){
		$this->email = $newEmail;
	}

	public function setCuit($newCuit){
		$this->cuit = $newCuit;
	}

	public function setIdProveedor($newId){
		$this->id = $newId;
	}

/*
	public function getNombreXid($idProd){
		$con=ConexionBD::obtenerInstancia();
		$sql = "SELECT nombre From productos Where id=".$idProd;
		$respuesta = $con->consultar($sql);
		return $respuesta;
	}
*/
	//------------ Demas funcion ------------
	public static function traerTodos(){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT * FROM proveedores";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		return $registro;
	} 

/*
	public static function traerTodosConStockMayor0(){
		$con=ConexionBD::obtenerInstancia();
		$sql="SELECT * FROM productos WHERE cantidadactual>0";
        $registro=ConexionBD::obtenerInstancia()->consultar($sql);
		return $registro;
	} 
*/
	public function traerProveedor($id){
	//aca se ve si el id es nulo, es error, si el id es distinto de nulo lo que hay que hacer es un DELETE

		$sql = "SELECT * FROM proveedores WHERE id=".$id;
	        $respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
	        //die($sql);
	        return $respuesta;
	}


	public function guardar(){
		//aca se ve si el id es nulo, se hace la carga como si fuera nuevo INSERT, si el id es distinto de nulo lo que hay que hacer es un UPDATE
		if($this->id == null){
			//die($this->nombre);

			$sql = "INSERT INTO proveedores(id,razonsocial,telefono,email,cuit) VALUES (NULL,'{$this->razonSocial}','{$this->telefono}','{$this->email}','{$this->cuit}')";
			//die($sql);
			ConexionBD::obtenerInstancia()->consultar($sql);

		} else {

			$sql = "UPDATE proveedores SET razonsocial='{$this->razonSocial}' ,telefono='{$this->telefono}', email='{$this->email}' , cuit=$this->cuit WHERE id='{$this->id}'";
			ConexionBD::obtenerInstancia()->consultar($sql);
		}

	} 
/*
	public function actualizarCantidad(){
		$sql = "UPDATE productos SET cantidadactual=$this->cantidad WHERE id=".$this->id;
	        $respuesta=ConexionBD::obtenerInstancia()->consultar($sql);
	}
*/
	public function eliminar(){
	//aca se ve si el id es nulo, es error, si el id es distinto de nulo lo que hay que hacer es un DELETE
		if($this->id == null){
			//MOSTRAR ERROR
			die("error");
		} else {
			$sql = "UPDATE proveedores SET borrado=1 WHERE id=".$this->id;
			ConexionBD::obtenerInstancia()->consultar($sql);

		}
	}



}

?>