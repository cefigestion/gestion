<?php
session_start();

require_once('Clases/Persistencia/ConexionBD.php');

//Orden sera la orden enviada por ajax por el parametro t
$orden=$_POST['t'];

//Inicializo respuesta
$respuesta=array('err'=>0,'txerr'=>'');

//SWITCH DE CONTROL
switch($orden){
  //--------------------------------------------------------------------------------------------------- Comprobacion de usuarios y sesion ------------------
    case 'ingresar':
            $dni=$_POST['dni'];
            $contrasena=$_POST['contrasena'];
            //VERIFICO QUE EXISTA EL USUARIO
            $sql="select count(*) as cant from usuarios where dni='$dni' and pass='$contrasena'";
            $registro=ConexionBD::obtenerInstancia()->consultar($sql);
            $array_user=armarArrayCon($registro)[0];
            if(isset($_SESSION['dni'])){
                session_destroy();
              }

            if($array_user['cant']>0)
            {

                $sql="select * from usuarios where dni='$dni' and pass='$contrasena'";
                $registro=ConexionBD::obtenerInstancia()->consultar($sql);
                $array_user=armarArrayCon($registro)[0];
                if($array_user['bloqueado']==1)
                {

                    $mensaje="Este usuario a sido bloqueado, por favor contactarse con el CEFI.";
                    $error=4;
                  
                }
                else
                {
                  $_SESSION['tipo']=$array_user['tipo'];
                  $_SESSION['dni']=$array_user['dni'];
                  $_SESSION['id']=$array_user['id'];
                  $mensaje="";
                  $error=0;

                }
              
            }
            else {
              $mensaje="Combinación de usuario y contraseña incorrecta, intente de nuevo.";
              $error=2;
            }

          $respuesta=array('err'=>$error, 'msg'=>$mensaje);

          break;  
  case 'logout':
          if($_SESSION['dni']!=''){
            $_SESSION=array();
            session_destroy();
            $respuesta=array('err'=>0, 'state'=>true);
          }
          else {
            $respuesta=array('err'=>1, 'state'=>false);
          }
          break;

  case "cerrarSesion":
      //LOGOUT
      $_SESSION=array();
      session_destroy();
      break;

  case "checkUser":
            if(isset($_SESSION['dni']))
            {
              switch ($_SESSION['tipo']) {
                case '0'://ADMIN
                  $respuesta=array('err'=>0, 'state'=>true, 'permiso'=>'Administrador');
                  break;
                case '1'://VENDEDOR
                  include('Clases/Logica/Caja.php');
                  $existeCaja=Caja::cajaAbiertaSegunIdUsuario($_SESSION['id']);
                  $respuesta=array('err'=>0, 'state'=>true, 'permiso'=>'Vendedor', 'existeCaja'=> "$existeCaja");
                  break;
                
              }
            }
            else {
              $respuesta=array('err'=>1, 'state'=>false);
            }
            break;


  //--------------------------------------------------------------------------------------------------- Terminal de ventas ------------------
    case "abrirCaja":
            if(isset($_SESSION['dni']) && $_SESSION['tipo']==1)
            {
              include('Clases/Logica/Caja.php');
              if(Caja::hayAbierta())
                $respuesta=array('err'=>2, 'msg'=> 'Ya se encuentra una caja abierta.');
              else if ($_POST['montoInicial']<=0) {
                $respuesta=array('err'=>3, 'msg'=> 'Monto inicial invalido.');
              }
              else{
                $montoInicial=$_POST['montoInicial'];
                $idUsuario=$_SESSION['id'];
                $rta=Caja::abrirCaja($montoInicial,$idUsuario);
                if($rta!=true)
                  $respuesta=array('err'=>1, 'msg'=> $rta);
              }
            }
            else
              $respuesta=array('err'=>1, 'msg'=> 'No hay sesion abierta.');
            break;

    case "cerrarCaja":
    if(isset($_SESSION['dni']) && $_SESSION['tipo']==1)
    {
      include('Clases/Logica/Caja.php');
      $montoFinal=$_POST['montoFinal'];
      Caja::cerrarCaja($montoFinal);

      //LOGOUT
      $_SESSION=array();
      session_destroy();
    }
    else
      $respuesta=array('err'=>1, 'msg'=> 'No hay sesion abierta.');
    break;
    case "cerrarVenta":
    include('Clases/Logica/Producto.php');
    include('Clases/Logica/Venta.php');
    include('Clases/Logica/ProductoVenta.php');
    include('Clases/Logica/Caja.php');

    $productos=json_decode($_POST['productos']);
    $cantidades=json_decode($_POST['cantidades']);

    $p= new Producto();
    $total=0;
    for($i=0;$i<sizeof($productos);$i++)
    {
      $p->setIdProducto($productos[$i]);
      $producto=armarArrayCon($p->traerProducto());
      $total+=$producto[0]['precioventa']*$cantidades[$i];
    }

    $v=new Venta();
    $v->inicializar();
    $v->setIdCaja(Caja::idCajaAbierta());
    $v->setTotal($total);
    $v->guardar();
    $id=$v->getId();

    $p= new Producto();
    for($i=0;$i<sizeof($productos);$i++)
    {
      $pv= new ProductoVenta(null,$id,$productos[$i]);

      $p->setIdProducto($productos[$i]);

      $pv->setPrecioVenta(armarArrayCon($p->traerProducto())[0]['precioventa']);
      $pv->setCantidad($cantidades[$i]);
      $pv->guardar();

      $nuevacant=armarArrayCon($p->traerProducto())[0]['cantidadactual']*1-$cantidades[$i];
      $p->setCantidadProducto($nuevacant);
      $p->actualizarCantidad();
    }
    break;
    case "guardarRetiro":
    include('Clases/Logica/Retiro.php');
    include('Clases/Logica/Caja.php');
            $dni=$_POST['dni'];
            $contraseña=$_POST['contraseña'];
            //VERIFICO QUE EXISTA EL USUARIO
            $sql="select count(*) as cant from usuarios where dni='$dni' and pass='$contraseña'";
            $registro=ConexionBD::obtenerInstancia()->consultar($sql);
            $array_user=armarArrayCon($registro)[0];
            if(!isset($_SESSION['dni'])){
                    $mensaje="No hay usuario solicitador.";
                    $error=6;
              }
            else if($array_user['cant']>0)
            {

                $sql="select * from usuarios where dni='$dni' and pass='$contraseña'";
                $registro=ConexionBD::obtenerInstancia()->consultar($sql);
                $array_user=armarArrayCon($registro)[0];
                if($array_user['bloqueado']==1)
                {

                    $mensaje="Este usuario a sido bloqueado, por favor contactarse con el CEFI.";
                    $error=4;
                  
                }
                else if($array_user['tipo']!=0)
                {

                    $mensaje="Este usuario no tiene acceso a Retiros.";
                    $error=5;
                  
                }
                else
                {
                    $r= new Retiro();
                    $r->inicializar();
                    $r->setIdCaja(Caja::idCajaAbierta());
                    $r->setIdUsuario($array_user['id']);
                    $r->guardar();
                    $error=0;
                    $mensaje='Todo Ok';

                }
              
            }
            else {
              $mensaje="Combinación de usuario y contraseña incorrecta, intente de nuevo.";
              $error=2;
            }

          $respuesta=array('err'=>$error, 'msg'=>$mensaje);

    break;
    case "initTerminalVenta":
    if(isset($_SESSION['dni']) && $_SESSION['tipo']==1)
    {
      include('Clases/Logica/Producto.php');
      $productos=Producto::traerTodosConStockMayor0();
      $respuesta=array('err'=>'0', 'productos'=> armarArrayCon($productos));
    }
    else
      $respuesta=array('err'=>1, 'msg'=> 'No hay sesion abierta.');
    break;
    case "trearProximaVenta":
            require_once("Clases/Logica/Venta.php");
            $id = Venta::trearProximaVenta();
            $respuesta=array('err'=>0, 'numero'=> $id);
            break;


  //--------------------------------------------------------------------------------------------------- Productos ------------------

    case "guardarProductos":
          require_once("Clases/Logica/Producto.php");

            $arrayNombre = json_decode($_POST['arrayNombre']);
            $arrayPrecioCompra = json_decode($_POST['arrayPrecioCompra']);
            $arrayporcentajeVenta = json_decode($_POST['arrayporcentajeVenta']);
            $arrayCantidad = json_decode($_POST['arrayCantidad']);
            $arrayPrecio = json_decode($_POST['arrayPrecio']);

            for($i = 0; $i<sizeof($arrayNombre) ; $i++){
              $td = new Producto();
              $td->setNombreProducto($arrayNombre[$i]);
              $td->setCantidadProducto($arrayCantidad[$i]);
              $td->setPrecioProducto($arrayPrecio[$i]);
              $td->setPrecioCompraProducto($arrayPrecioCompra[$i]);
              $td->setComisionProducto($arrayporcentajeVenta[$i]);

              $td->guardar();
            }
          break;



    case "traerTodosProductos":
          require_once("Clases/Logica/Producto.php");
          $productos = Producto::traerTodos();
          $respuesta = array('err'=>0, 'productos'=>armarArrayCon($productos));
          break;


    case "eliminarProducto":
        require_once('Clases/Logica/Producto.php');
        $p=new Producto();
        $p->eliminar();
        $respuesta=array('err'=>0);
        break;
    case "traerProducto":
        require_once("Clases/Logica/Producto.php");
        $p=new Producto();
        $rta=$p->traerProducto();
        $respuesta=array('err'=>0, 'producto'=>armarArrayCon($rta));
        break;


  //--------------------------------------------------------------------------------------------- Usuarios ----------------------------
    case "traerUsuarioActual":
            require_once("Clases/Logica/Usuario.php");
            $t = new Usuario();
            $t->setIdUsuario($_SESSION['id']);
            $rta = $t->traerUsuario();
            $respuesta=array('err'=>0, 'usuario'=>armarArrayCon($rta));
            break;

    case "guardarUsuario":
            require_once("Clases/Logica/Usuario.php");
            $t = new Usuario();
            $t->guardar();
            break;

    case "traerTodosUsuarios":
            require_once("Clases/Logica/Usuario.php");
            $usuarios = Usuario::traerTodos();
            $respuesta= array('err' => 0 , 'usuarios'=>armarArrayCon($usuarios) );
            break;

    case 'traerUsuario':
            require_once("Clases/Logica/Usuario.php");
            $usuario = new Usuario();
            $rta = $usuario->traerUsuario();
            $cajas=$usuario->traerCajas();
            $respuesta = array('err'=>0, 'usuario'=>armarArrayCon($rta), 'cajas'=>armarArrayCon($cajas));
            break;

    case "bloquearUsuario":
            require_once("Clases/Logica/Usuario.php");
            $u = new Usuario();
            $u->bloquear();
            break;

    case "desbloquearUsuario":
            require_once("Clases/Logica/Usuario.php");
            $u= new Usuario();
            $u->desbloquear();
            break;

    //case "traerCajasSegunUsuario":


  //-----------------------------------------------------------------------------------------Reportes/Resumenes---------------------------------
    case "traerTodosRegistros":
            require_once("Clases/Logica/Registro.php");
            $registros=Registro::traerTodos();
            $respuesta=array('err'=>0, 'registros'=>armarArrayCon($registros));
            break;

    case "traerTodosCajas":
            require_once("Clases/Logica/Caja.php");
            $registros=Caja::traerTodos();
            $respuesta=array('err'=>0, 'cajas'=>armarArrayCon($registros));
            break;

    case "mostrarCaja":
            require_once("Clases/Logica/Caja.php");
            require_once("Clases/Logica/Venta.php");
            require_once("Clases/Logica/Usuario.php");
            $idCaja=$_REQUEST['idCaja'];
            /////////Traigo datos de la caja
            $datosCaja=armarArrayCon(Caja::traerCaja($idCaja)); //VER SI ESTO CONVIENE ASI O TRABAJAR CON EL CONSTRUCTOR DE CAJA Y LOS $_POST
            //////////Traigo datos del usuario de esa caja
            $usuario=new Usuario();
            $usuario->setIdUsuario(($datosCaja[0]['idusuario'])); 
            $u=$usuario->traerUsuario();
            //Traigo los retiros para esa caja
            $retiros=armarArrayCon(Caja::traerRetirosDeCaja($idCaja));
            //Traigo el usuario del retiro para mostrarlo tambien
            for($i=0; $i<sizeof($retiros); $i++)
            {
              $usuario=new Usuario();
              $usuario->setIdUsuario($retiros[$i]['usuarios_id']);
              $retiros[$i]['usuario']=armarArrayCon($usuario->traerUsuario());
            }
            //Traigo las ventas para esa caja
            $ventas=armarArrayCon(Caja::traerVentasDeCaja($idCaja));
            //Para cada venta, traigo los productos vendidos
            for($i=0; $i<count($ventas);$i++)
            {
              $ventas[$i]['productos']=armarArrayCon(Venta::traerProductosDeVenta($ventas[$i]['id']));
            }
            //Armo la respuesta json
            $respuesta=array('err'=>0, 'caja'=>$datosCaja, 'usuario'=>armarArrayCon($u), 'retiros'=>$retiros, 'ventas'=>$ventas);
            break;
}

echo json_encode(validar($orden,$respuesta));

function armarArrayCon($resDb){
        $array=array();
        if($resDb!=false){  //CONDICION PARA QUE NO EXPLOTE SI NO ENCUENTRA REGISTROS
              while($datos  = $resDb->fetch_assoc()){
                      $array[] = $datos;
              }
        }
        return $array;
}

function validar($tarea ,$respuestaLista)
{
    /*VALIDA LA TAREA SOLICITADA SEGUN EL USUARIO QUE LO SOLICITA*/
    $tareasNoLogueado=array("ingresar");
    $tareasVendedor=array("ingresar","logout","cerrarSesion","checkUser", "abrirCaja","cerrarCaja","cerrarVenta", "guardarRetiro","initTerminalVenta","trearProximaVenta","traerUsuarioActual");
    $tareasAdministrador=array("ingresar","logout","cerrarSesion","checkUser","guardarProductos","traerTodosProductos","eliminarProducto","traerProducto","traerUsuarioActual","guardarUsuario","traerTodosUsuarios","traerUsuario","bloquearUsuario","desbloquearUsuario","traerTodosRegistros","traerTodosCajas","mostrarCaja");
    
    if(estaLogueado())
    {
      if($_SESSION['tipo']==1)
      {
        if(in_array($tarea,$tareasVendedor))
          return $respuestaLista;
        else
          return array('err'=>0, 'permiso'=>'DENEGADO1');  //EN ESTE CASO, ACCEDE A UNA TAREA QUE NO LE CORRESPONDE
      }
      elseif($_SESSION['tipo']==0)
      {
        if(in_array($tarea,$tareasAdministrador))
          return $respuestaLista;
        else
          return array('err'=>0, 'permiso'=>'DENEGADO2');  //EN ESTE CASO, ACCEDE A UNA TAREA QUE NO LE CORRESPONDE
      }
      else //EN ESTE CASO, SESSION TIENE UN TIPO DISTINTO DE 0 Y 1, ES UN HACKEO 
      { 
        return array('err'=>0, 'permiso'=>'DENEGADO3');
      }
  }
  else //EN ESTE CASO, NO ESTA LOGUEADO
    {
      if(in_array($tarea,$tareasNoLogueado))
        return $respuestaLista;
      else
        return array('err'=>0, 'permiso'=>'DENEGADO4');
    }
}


function estaLogueado(){
  if(isset($_SESSION) && isset($_SESSION['dni']) && isset($_SESSION['tipo']))
    return true;
  else
    return false;
}